# bbsVue3

项目介绍：
本项目是毕业设计作品，基于NodeJs开发的一个BBS论坛系统，采用了MongoDB作为数据库，简单模拟了用户进入论坛系统时的场景。

系统开发环境
node.js			v14.16.0
@vue/cli		v4.5.8
npm			v7.8.0
mongodb		        v4.4.4

1.系统安装及注意事项
本系统基于Visual Studio Code编译器进行编译，使用Microsoft Edge浏览器进行调试。不同的浏览器可能显示效果不同，可在后期进行兼容性开发。本系统可以在Linux、Windows、Mac等支持Web的操作系统下运行。

解压项目后，分别进入后端目录bbs-mgr-be、前端目录bbs-mgr-fe下的package.json文件，里面配置了详细的安装包及对应的版本号。Dependencies指运行时依赖，devDependencies指开发时依赖。

运行前需使用npm命令在终端进行对应安装，切记不可使用npm i 进行全部安装。若安装失败，可以尝试使用cnpm淘宝镜像进行相关命令安装。若仍然不可安装或运行错误，则分别将文件下的node_modules依赖包删除后重新安装直至没有错误。

安装成功后，进入前端文件夹bbs-mgr-fe下运行npm run serve开启前端端口，一般为8080。进入后端文件夹bbs-mgr-be下运行npm run dev开启后端端口，一般为3000。本地安装http-server，然后分别进入imgs、other_imgs文件夹下，在终端使用http-server开启端口，imgs对应8081，other_imgs对应8082。

点击前端端口便可以在浏览器中进行预览。

2.系统主要目录及文件含义
Assets是本地静态文件夹，components是公共组件文件夹，config是一些配置文件夹，helpers是公共函数方法文件夹，router是路由文件夹，store是全局参数配置文件夹，service是http请求文件夹，views是视图文件夹，main.js是主要入口，project.config.js是静态公共配置文件，intit用作快速添加数据库数据，使用node命令+文件名，upload是存储excel文件的文件夹。

希望可以帮助你