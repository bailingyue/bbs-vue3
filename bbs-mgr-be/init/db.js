const mongoose = require('mongoose');
const { connect } = require('../src/db/index');

const { defaultCharacters } = require('../src/helpers/character');

const Character = mongoose.model('Character');

connect()
    .then(async () => {
        await Character.insertMany(defaultCharacters);
        console.log('角色集合初始化');
    })
