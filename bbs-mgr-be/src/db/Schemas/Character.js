const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const CharacterSchema = new mongoose.Schema({
    name: String,
    title: String,
    power: Object,
    meta: getMate()
});
CharacterSchema.pre('save', preSave);
mongoose.model('Character', CharacterSchema);
