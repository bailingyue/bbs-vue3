const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const CommentSchema = new mongoose.Schema({
    postId: String,
    author: String,
    content: Object,
    classify: String,
    likes: Number,
    status: Number,
    isAdd: Number,
    meta: getMate()
});
CommentSchema.pre('save', preSave);
mongoose.model('Comment', CommentSchema);
