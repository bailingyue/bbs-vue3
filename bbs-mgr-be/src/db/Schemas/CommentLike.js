const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const CommentLikeSchema = new mongoose.Schema({
   commentId: String,
   userList: Array, 
    meta: getMate()
});

CommentLikeSchema.pre('save', preSave);

mongoose.model('CommentLike', CommentLikeSchema);