const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const InviteCodeSchema = new mongoose.Schema({
    // 邀请码
    code: String,
    // 注册用户
    user: String,
    
    meta: getMate()
});
InviteCodeSchema.pre('save', preSave);
mongoose.model('InviteCode', InviteCodeSchema);