const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const LogSchema = new mongoose.Schema({
    // type: String,
    postId: String,
    content: String,
    account: String,
    classify: String,
    meta: getMate()
});

LogSchema.pre('save', preSave);

mongoose.model('Logs', LogSchema);