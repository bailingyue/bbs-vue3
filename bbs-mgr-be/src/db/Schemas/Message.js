const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const MessageSchema = new mongoose.Schema({
    type: String,
    postId: String,
    title: String,
    sendUser: String,
    ReceiveUser: String,
    content: String,
    classify: String,
    // 1为显示 0为隐藏
    status: Number,
    meta: getMate()
});

MessageSchema.pre('save', preSave);

mongoose.model('Message', MessageSchema);