const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const ModerApplySchema = new mongoose.Schema({
    account: String,
    classify: String,
    content: String,
    // 0 待处理
    // 1 已拒绝
    status: Number,
    meta: getMate()
});

ModerApplySchema.pre('save', preSave);

mongoose.model('ModerApply', ModerApplySchema);
