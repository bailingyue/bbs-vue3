const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const NoticeSchema = new mongoose.Schema({
    title: String,
    author: String,
    content: String,
    classify: String,
    status: Number,
    meta: getMate()
});
NoticeSchema.pre('save', preSave);
mongoose.model('Notice', NoticeSchema);
