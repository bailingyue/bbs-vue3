const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const PostSchema = new mongoose.Schema({
    // 帖子名称
    name: String,
    // 发帖人
    author: String,
    // 发帖内容
    content: String,
    // 发帖时间
    date: String,
    // 分类
    classify: String,
    // 评论数
    comments: Number,
    // 点赞数
    likes: Number,
    // 置顶状态
    status: Number,
    // 表示是否置顶后 拿到积分
    // 0 没有置顶
    // 1 置顶获得分 不变
    isAdd: Number,
    meta: getMate(),
});
PostSchema.pre('save', preSave);
mongoose.model('Post', PostSchema);