const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const PostClassifySchema = new mongoose.Schema({
    title: String,
    content: String,
    moderators: Array,
    meta: getMate()
});

PostClassifySchema.pre('save', preSave);

mongoose.model('PostClassify', PostClassifySchema);