const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const PostLikeSchema = new mongoose.Schema({
   postId:String,
   userList: Array, 
    meta: getMate()
});

PostLikeSchema.pre('save', preSave);

mongoose.model('PostLike', PostLikeSchema);