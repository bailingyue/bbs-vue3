const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const RecordSchema = new mongoose.Schema({
    user: {
        account: String,
        id: String
    },
    request: {
        method: String,
        url: String,
        status:Number
    },

    startTime: Number,
    endTime: Number,
    show: Boolean,
    meta: getMate()
});

RecordSchema.pre('save', preSave);

mongoose.model('Records', RecordSchema);