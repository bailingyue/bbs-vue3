const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const ResetPasswordSchema = new mongoose.Schema({
    account: String,
    // 1 待处理
    // 2 已重置
    // 3 已忽略
    status: Number,
    meta: getMate()
});

ResetPasswordSchema.pre('save', preSave);

mongoose.model('ResetPassword', ResetPasswordSchema);
