const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const ResponseSchema = new mongoose.Schema({
    recordId: String,
    data: String,
    meta: getMate()
});

ResponseSchema.pre('save', preSave);

mongoose.model('Response', ResponseSchema);
