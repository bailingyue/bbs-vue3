const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const UserSchema = new mongoose.Schema({
    account: String,
    password: String,
    character: String,
    classify: String,
    meta: getMate()
});
UserSchema.pre('save', preSave);
mongoose.model('User', UserSchema);