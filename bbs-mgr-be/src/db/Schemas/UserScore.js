const mongoose = require('mongoose');
const {getMate, preSave} = require('../helpers.js');

const UserScoreSchema = new mongoose.Schema({
    userId:String,
    score:Number,
    meta: getMate()
});

UserScoreSchema.pre('save', preSave);

mongoose.model('UserScore', UserScoreSchema);