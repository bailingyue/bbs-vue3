const getMate = () => {
    return {
        // 何时被创建
        createdAt:{
            type:Number,
            default:(new Date()).getTime()
        },

        // 何使被更新
        updatedAt:{
            type:Number,
            default: (new Date()).getTime(),
        },
    }
};

// 用于更新时间
const preSave = function(next){
    if(this.isNew){
        const ts = Date.now();

        this['meta'].createdAt = ts;
        this['meta'].updatedAt = ts;
    }else{
        this['meta'].updatedAt = Date.now();
    }
    next();
}

module.exports = {
    getMate,
    preSave
}