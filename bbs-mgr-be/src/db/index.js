const mongoose = require('mongoose');
require('./Schemas/User.js');
require('./Schemas/InviteCode.js');
require('./Schemas/Post.js');
require('./Schemas/Logs.js');
require('./Schemas/Character.js');
require('./Schemas/Records.js');
require('./Schemas/Response.js');
require('./Schemas/ResetPassword.js');
require('./Schemas/PostClassify.js');
require('./Schemas/Notices.js');
require('./Schemas/ModerApply.js');
require('./Schemas/PostLike.js');
require('./Schemas/Comment.js');
require('./Schemas/CommentLike.js');
require('./Schemas/UserScore.js');
require('./Schemas/Message.js');


const connect = () => {
    return new Promise((resolve) => {
        // 连接数据库
        mongoose.connect('mongodb://127.0.0.1:27017');
        // 当数据库被打开的时候 做一些事情
        mongoose.connection.on('open', ()=>{
            console.log('连接数据库成功');
            resolve();
        });
    })
};

module.exports = {
    connect
}
