`  -1 无任何权限
    0 管理员权限
    1 增加权限
    2 删除权限
    3 查找权限
    4 修改权限
    5 置顶权限
`

const defaultCharacters = [{
        title: '管理员',
        name: 'admin',
        power: {
            post: [0],
            classify: [0],
            comment: [0],
            notices: [0],
            user: [0],
            other:[0]
        }
    },
    {
        title: '版主',
        name: 'moderator',
        power: {
            post: [0],
            classify: [-1],
            comment: [1, 2, 3, 5],
            notices: [1, 2, 4, 5],
            user: [-1],
            other:[-1]
        }
    },
    {
        title: '普通用户',
        name: 'member',
        power: {
            post: [1, 3],
            classify: [-1],
            comment: [1],
            notices: [-1],
            user: [-1],
            other:[-1]
        }
    },
];

module.exports = {
    defaultCharacters
}