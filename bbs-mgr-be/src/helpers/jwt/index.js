const jwt = require('jsonwebtoken');
const config = require('../../project.config');
const koaJwt = require('koa-jwt');
const mongoose = require('mongoose');
const User = mongoose.model('User');

const getToken = (ctx) => {
    let { authorization } = ctx.header;
    return authorization.replace('Bearer ', '').replace('bearer ', '');
}

const verify = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, config.JWT_SECRET, (err, payload) => {
            if(err){
                reject(err);
                return;
            }
            resolve(payload);

        });
        
    });
};

// 中间件
const middleware = (app) => {
    // koa-jwt 路径请求进行检查
    app.use(koaJwt({
        secret: config.JWT_SECRET
    }).unless({
        // 下方路径不予检查
        path:[
            /^\/auth\/login/,
            /^\/auth\/register/,
            /^\/reset-password\/add/
        ]
    }));
};

// 返回相应错误
const resError = (ctx) => {
    ctx.status = 401;
    ctx.body = {
        code: 0,
        msg: '用户校验失败'
    };
};

// 校验用户信息是否存在
// 例如 数据库中修改后 后台需要校验
const checkUser = async (ctx, next) => {
    // 由于跳过了检测, 故在没有token情况下会出错, 需单独处理
    const { path } = ctx;
    if(path === '/auth/login' || path === '/auth/register' || path === '/reset-password/add'){
        await next();
        return;
    }

    const { _id, account, character} = await verify(getToken(ctx));

    const user = await User.findOne({
        _id
    }).exec();

    if(!user){
        resError(ctx);
        return;
    }

    if(account !== user.account){
        resError(ctx);
        return;
    }

    if(character !== user.character){
        resError(ctx);
        return;
    }

    await next()
}

// 处理错误
const catchTokenError = async (ctx, next) => {
    // next() async函数
    return next().catch((error)=> {
        if(error.status == 401){
            ctx.status = 401;
            ctx.body = {
                code: 0,
                msg: 'token error'
            };
        }else{
            throw error;
        }
    })
}

module.exports = {
    verify,
    getToken,
    middleware,
    catchTokenError,
    checkUser
}