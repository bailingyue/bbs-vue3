const { verify, getToken } = require('../jwt');
const mongoose = require('mongoose');
const Records = mongoose.model('Records');
const Response = mongoose.model('Response');

const logMiddleware = async (ctx, next) => {
    const startTime = Date.now();
    await next();

    let payload = {};
    // promise可能会出错
    try{
        payload = await verify(getToken(ctx));
    } catch(e) {
        payload = {
            account: '未知用户',
            id: ''
        };
    }

    const url = ctx.url;
    const method = ctx.method;
    const status = ctx.status;
    let show = true;

    if(url === '/records/delete'){
        show = false;
    }
    
    // console.log(url, payload);
    let responseBody = '';
    if(typeof ctx.body == 'string'){
        responseBody = ctx.body
    }else{
        try{
            responseBody  = JSON.stringify(ctx.body);
        }catch{
            responseBody = '';
        }
    }
    const endTime = Date.now();
    const records = new Records({
        user: {
            account: payload.account,
            id: payload._id
        },
        request: {
            url,
            method,
            status,
        },
        show,
        startTime,
        endTime
    });
    
    await records.save();

    const response = new Response({
        recordId: records._id,
        data: responseBody
    })

    await response.save()

}

module.exports = {
    logMiddleware
}