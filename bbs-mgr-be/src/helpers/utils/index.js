const getBody = (ctx) => {
    return ctx.request.body || {};
};

const tsPadStart = (str) => {
    str = String(str);
    return str.padStart(2, '0')
  };
  
const formatTimestamp = (ts) => {
    const date = new Date(Number(ts));
  
    const YYYY = date.getFullYear();
    const MM = tsPadStart(date.getMonth() + 1);
    const DD = tsPadStart(date.getDate());
    const hh = tsPadStart(date.getHours());
    const mm = tsPadStart(date.getMinutes());
    const ss = tsPadStart(date.getSeconds());
    return `${YYYY}/${MM}/${DD} ${hh}:${mm}:${ss}`;
}

module.exports = {
    getBody,
    formatTimestamp
}

