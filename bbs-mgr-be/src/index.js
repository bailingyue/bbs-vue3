const Koa = require('koa');
// 中间键
const koaBody = require('koa-body');
const app = new Koa();
const {connect} = require('./db/index.js');
const registerRoutes = require('./routers/index.js');
const cors = require('@koa/cors');
const { middleware: koaJwtMiddleware, catchTokenError ,checkUser} = require('./helpers/jwt');
const { logMiddleware }  = require('./helpers/record');

connect().then(() => {
    // 解决跨域问题
    app.use(cors());
    // 方便获得请求体的数据
    app.use(koaBody({
      multipart: true,
      formidable: {
        maxFieldsSize: 200 * 1024 * 1024,
      }
    }));
    // 处理错误, 隐藏不必要的错误
    app.use(catchTokenError);
    // jwttoken检查
    koaJwtMiddleware(app);
    // 拿到用户信息
    app.use(checkUser);
    // 注册日志
    app.use(logMiddleware);
    // 注册路由
    registerRoutes(app);
    // 开启http服务
    app.listen(3000, () => {
      console.log('启动成功！')
    });
});

