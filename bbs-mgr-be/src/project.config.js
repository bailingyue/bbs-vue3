const path = require('path');

module.exports = {
    DEFAULT_PASSWORD:'111111',
    JWT_SECRET: 'bbs-mgr',
    UPLOAD_DIR: path.resolve(__dirname, '../upload'),
    DEFAULT_INTERATION: '10',
    APPLY_MODERATER_SCORE: '200',
    SCORE_TOP: 20,
    SCORE_LIKE: 10,
    IMG_DIR: path.resolve(__dirname, '../imgs'),
    // 路径转为http
    IMG_HTTP: 'http://127.0.0.1:8081'
}