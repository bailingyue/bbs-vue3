const Router = require('@koa/router');
const mongoose = require('mongoose');
const {getBody} = require('../../helpers/utils');
const Comment = mongoose.model('Comment');
const User = mongoose.model('User');
const Post = mongoose.model('Post');
const CommentLike = mongoose.model('CommentLike');
const UserScore = mongoose.model('UserScore');
const Message = mongoose.model('Message');

const config = require('../../project.config');


const {verify, getToken} = require('../../helpers/jwt');

const router = new Router({
    prefix: '/comment',
});

const getUserScoreObject = async (ctx, account) => {
    // 查询数据是否存在
    const fineuser = await User.findOne({
        account
    })
    if(!fineuser){
        ctx.body = {
            code: 0,
            msg: '用户不存在',
        }
        return;
    }

      const fineOne = await UserScore.findOne({
          userId: fineuser._Id
      })
      if(!fineOne){
          const res = new UserScore ({
              userId,
              score: 0
          })
  
          await res.save();   
          ctx.body = {
              data: res,
              code: 1
          }
          return;
      }
  
      return fineOne;
}

// 处理置顶
const showComment = async (status = 0, postId, bySort, _filter) => {
    if(!bySort){
        bySort = {
            _id: -1
        }
    }else{
        bySort = {
            likes: -1
        }
    }
    let tempAnd = [];
    if(postId){
        tempAnd = [
            {status},
            {postId}
        ]
    }else{
        tempAnd = [
            {status}
        ]
    }
    const showJudge = {
        $and: tempAnd
    }

    if(_filter){
        showJudge['$and'].push(_filter)
    }

    const list = await Comment
    .find(showJudge)
    .sort(bySort)
    .exec();

    return list;
}
// 主要针对前端 公告、板块展示需要的
router.get('/list', async (ctx) => { 
    const {
        keyword,
        postId,
        bySort
    } = ctx.query;

    if (keyword) {
        //正则匹配 i忽略大小写
        let reg = new RegExp(keyword, "i");
        var _filter = {
            //多字段匹配
            $or: [
              {postId: {$regex: reg}},
              {author: {$regex: reg}},
              {content: {$regex: reg}},
            ]
        }
    }
 

    const list_F = JSON.parse(JSON.stringify(await showComment(1, postId, bySort, _filter)));
    const list_B = JSON.parse(JSON.stringify(await showComment(0, postId, bySort, _filter)));
    const list = list_F.concat(list_B);

    const user = await verify(getToken(ctx));
        if(!user){
            ctx.body = {
                code: 0,
                msg: '访问非法',
                data: null
            }
            return;
        }

        // for of 等无法修改原数组的值 
        for(let i = 0; i < list.length;  i++){
            // 找到每个帖子找到对应状态
            const one = await CommentLike.findOne({
                commentId: list[i]._id
            })
            // 如果没有 则无人点赞 isLike置为0
            if(!one){

                list[i].isLike = 0
                continue;
            }

            // 有 循环判断user是否点赞
            const index = await one.userList.findIndex(itemId => itemId == user._id);
            if(index != -1){
                list[i].isLike = 1
            }else{
                list[i].isLike = 0
            }
        }

    const total = await Comment.countDocuments({
        postId
    }).exec();

    // 循环整个list 遇到isAdd=0且likes>xx 则score+10 并将isAdd置为1
    for(let i = 0; i < list.length; i++){
        if(list[i].likes > config.SCORE_LIKE && !list[i].isAdd){
            const scoreObject = await getUserScoreObject(ctx, list[i].author);
            scoreObject.score += config.SCORE_LIKE;
            await scoreObject.save();
            list[i].isAdd = 1;
        }
        
    }

    console.log(list)


    ctx.body = {
        data: {
            list,
            total
        },
        msg: '获取评论成功',
        code: 1,
    };

});
// 管理
router.get('/listAll', async (ctx) => {
    let {
       keyword
   } = ctx.query;

//    console.log(keyword)
   const searchF = {
       $and: [
           {status: 1},
       ] 
   };
   const searchB = {
       $and: [
           {status: 0},
       ]
   };

   if (keyword) {
       //正则匹配 i忽略大小写
       let reg = new RegExp(keyword, "i");
       var _filter = {
           //多字段匹配
           $or: [
             {postId: {$regex: reg}},
             {author: {$regex: reg}},
             {classify: {$regex: reg}},
             {content: {$regex: reg}},
           ]
       }
       searchF['$and'].push(_filter);
       searchB['$and'].push(_filter);
   }

   const list_forward = await Comment
       .find(searchF)
       .sort({
           _id: -1
       })
       .exec();

   const list_backward = await Comment
       .find(searchB)
       .sort({
           _id: -1
       })
       .exec();

   const list = list_forward.concat(list_backward);
//    console.log(list)

   ctx.body = {
       data: list,
       code: 1,
       msg: '获取列表成功',
   }
   _filter = null;
});

router.post('/add', async (ctx) => {
    const {
        postId,
        content,
        isManage
    } = getBody(ctx);

    let{
        author
    }= getBody(ctx);
    
    if(!postId || !content){
        return;
    }

    const findPost = await Post.findOne({
        _id: postId
    })
    if(!findPost){
        ctx.body = {
            code: 0,
            msg: '帖子不存在',
        };
        return;
    }
  
    if(!isManage){
        // 首页
        const res = await verify(getToken(ctx));
        if(!res){
            ctx.body = {
                code: 0,
                msg: '访问非法',
                data: null
            }
            return;
        }
        author = res.account;
    }else{
        // 管理台
        if(!author){
            ctx.body = {
                code: 0,
                msg: '用户不为空',
            };
            return;
        }
        
        const findUser = await User.findOne({
            account: author
        }).exec();
        if(!findUser){
            ctx.body = {
                data: null,
                code: 0,
                msg: '用户不存在',
            };
            return;
        }
    }

    const findComment = await Comment.findOne({
        postId,
        author,
        content
    }).exec();
    if(findComment){
        ctx.body = {
            code: 0,
            msg: '已存在该评论',
            data: null
        }
        return;
    }

    // console.log(author)

    const comment = new Comment({
        postId,
        author,
        content,
        classify: findPost.classify,
        status: 0,
        likes: 0,
        isAdd: 0
    })
    const resN = await comment.save();

    findPost.comments += 1;
    await findPost.save();
    console.log(resN)

    // 消息逻辑
    const newMessage =  new Message({
        type: 'post_comment',
        postId,
        title: findPost.name,
        sendUser: author,
        ReceiveUser: findPost.author,
        content,
        classify: findPost.classify,
        status:1,
    })
   
    await newMessage.save();
    

    ctx.body = {
        data: resN,
        code: 1,
        msg: '发布成功',
    };
});

router.post('/update', async (ctx) => {
    const {
        id,
        postId,
        furStatus,
        isForward,
        ...others
    } = getBody(ctx);
     // 声明是否点赞变量
     let status = 0;

    const findComment = await Comment.findOne({
        _id: id
    }).exec();
    if(!findComment){
        return;
    }

    const token = await verify(getToken(ctx));
    if(!token){
        return;
    }
    const author = token.account;
    const findPost = await Post.findOne({
        _id: postId
    })
    if(!findPost){
        ctx.body = {
            code: 0,
            msg: '帖子不存在',
        };
        return;
    }

    if(typeof furStatus != 'undefined'){
        const user = await verify(getToken(ctx));
        if(!user){
            ctx.body = {
                code: 0,
                msg: '访问非法',
                data: null
            }
            return;
        }
        const one = await CommentLike.findOne({
            commentId: id
        }).exec();
        // 已有人点赞 user将要点赞
        if(one && furStatus){
            // 查找user是否点赞 这里存放userId
            const index = await one.userList.findIndex(itemId => itemId == user._id);
            console.log("111111111111111")
            console.log(index)
            // user已点赞
            if(index != -1){
                ctx.body = {
                    code: 0,
                    msg: '您已点赞'
                }
                return;
            }
            // user未点赞
            one.userList.push(user._id);
            await one.save();
            findComment.likes += 1;
            console.log(findComment)
            await findComment.save();
            status = 1;
        // 已有人点赞 user将取消点赞
        }else if(one && !furStatus){
            console.log("2222222222222")

            const index = await one.userList.findIndex(itemId => itemId == user._id);
            // user未点赞
            if(index == -1){
                ctx.body = {
                    code: 0,
                    msg: '您尚未点赞'
                }
                return;
            }
            // user已点赞
            one.userList.splice(index, 1);
            await one.save();
            findComment.likes = findComment.likes - 1 > 0 ? findComment.likes -= 1 : 0;
            await findComment.save();
            status = 0;
        }
        // 无人点赞
        if(!one){
            console.log("333333333333")

            // user将要点赞
            if(furStatus){
                const userList = [];
                userList.push(user._id);
                const res = new CommentLike ({
                    commentId: id,
                    userList
                })
                await res.save();
                findComment.likes += 1;
                await findComment.save();
                status = 1;
                
            }else{
            // user将取消点赞
                ctx.body = {
                    code: 0,
                    msg: '出错了'
                }
                return;
            }
        }
    }
  
    const newQuery = {
        author
    };
    Object.entries(others).forEach(([key, value]) => {
        if (value) {
            newQuery[key] = value;
        }
    });

    Object.assign(findComment, newQuery);

    findComment.postId = postId;
    findComment.classify = findPost.classify;

    const res = await findComment.save();
    // console.log("res", res)

    if(isForward){
        // 消息逻辑
        const findMessage = await Message.findOne({
            type: 'post_comment_like',
            postId,
            sendUser: author,
            ReceiveUser: findComment.author,
        })
        if(!findMessage){
            const res = new Message({
                type: 'post_comment_like',
                postId,
                title: '',
                sendUser: author,
                ReceiveUser: findComment.author,
                content: findComment.content,
                classify: findComment.classify,
                status,
            });
            await res.save();
        }else{
            findMessage.status = status;
            await findMessage.save();
        }
    }
    if(newQuery['author']){
        const findUser = await User.findOne({
            account: newQuery['author']
        })
        if(!findUser){
            ctx.body = {
                data: null,
                msg: '不存在该用户',
                code: 0,
            };
            return;
        }
    }
    
    ctx.body = {
        msg: '更新成功',
        code: 1,
        data: res
    };
});

router.delete('/:id', async (ctx) => {
    const {
        id,
    } = ctx.params;

    const findComment = await Comment.findOne({
        _id: id
    });

    const findPost = await Post.findOne({
        _id: findComment.postId
    })

    findPost.comments =  findPost.comments - 1 > 0 ? findPost.comments -= 1 : 0;
    await findPost.save();

    await CommentLike.deleteMany({
        commentId: id
    })

    await Comment.deleteOne({
        _id: id
    });

    ctx.body = {
        msg: '删除成功',
        code: 1,
    };


});

router.post('/update/status', async (ctx) => {
    const {
        id
    } = getBody(ctx);

    let {
        status
    } = getBody(ctx);

    status = Number(status);

    const one = await Comment.findOne({
        _id: id
    }).exec();

    if(!one){
        ctx.body = {
            msg: '评论不存在',
            code: 0
        }
        return;
    }

     // 如果将置顶的帖子未曾得分 则置为1 且不变
     if(!one.isAdd){
        // 找到发帖用户
        const user = await User.findOne({
            account: one.author
        }).exec();

        const userScore = await UserScore.findOne({
            userId: user._id
        }).exec();

        userScore.score += config.SCORE_TOP;
        await userScore.save();
        // console.log(userScore)

        one.isAdd = 1;
        await one.save();
    }

    one.status = status;
    await one.save();

    let mes = '';
    if(status){
        mes = '置顶成功'
    }else{
        mes = '取消置顶'
    }

    ctx.body = {
        msg: mes,
        code: 1,
    }

});

module.exports = router;