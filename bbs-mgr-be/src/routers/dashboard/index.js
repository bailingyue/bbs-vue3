const Router = require('@koa/router');
const mongoose = require('mongoose');
const {getBody} = require('../../helpers/utils');

const User = mongoose.model('User');
const Post = mongoose.model('Post');
const Record = mongoose.model('Records');
const Comment = mongoose.model('Comment');
const fs = require('fs');
const config = require('../../project.config');


const router = new Router({
    prefix: '/dashboard',
});

router.get('/base-info', async (ctx) => {
    const userToal = await User.countDocuments();
    const postToal = await Post.countDocuments();
    const recordToal = await Record.countDocuments();
    const comment = await Comment.countDocuments();

    const list = {
        userToal,
        postToal,
        recordToal,
        comment
    }
    ctx.body = {
        msg: '获取成功',
        code: 1,
        data: list
    };
});

router.get('/pics', async (ctx) => {
    let srcList = [];
    const files = fs.readdirSync(config.IMG_DIR)
    files.forEach(function (item, index) {
        let src = config.IMG_HTTP + '/' + item
        let imgObject = {
            src: src,
            filename: item
        }
        srcList.push(imgObject)
    })

    ctx.body = {
        msg: '获取成功',
        code: 1,
        data: srcList
    };
});

module.exports = router;