const Router = require('@koa/router');
const mongoose = require('mongoose');
// const {getBody} = require('../../helpers/utils');
const Post = mongoose.model('Post');
const PostClassify = mongoose.model('PostClassify');

const router = new Router({
    prefix: '/datashow',
});

router.get('/list', async (ctx) => {
    const {
        title,
        id
    } = ctx.query;

    const findOne = await PostClassify.find({id}).exec();
    if(!findOne){
        ctx.body = {
            data: null,
            msg: `无法进入此模块`,
            code: 0,
        };
        return;
    }

    const list = await Post
        .find({id})
        .sort({
            _id: -1
        })
        .exec();
    
    const total = await Post.countDocuments({id}).exec();

    ctx.body = {
        data: {
            total,
            list
        },
        msg: `进入${title}板块`,
        code: 1,
    };

});


module.exports = router;