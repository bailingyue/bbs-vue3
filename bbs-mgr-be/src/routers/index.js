const auth = require('./auth')
const inviteCode = require('./invite-code')
const post = require('./post')
const logs = require('./logs')
const user = require('./user')
const character = require('./character')
const records = require('./records')
const resetPassword = require('./reset-password')
const postClassify = require('./post-classify')
const profile = require('./profile')
const dashboard = require('./dashboard')
const datashow = require('./datashow')
const upload = require('./upload')
const notices = require('./notices')
const moderApply = require('./moder-apply')
const comment = require('./comment')
const userScore = require('./user-score')
const message = require('./message')


module.exports = (app) => {
    app.use(auth.routes());
    app.use(inviteCode.routes());
    app.use(post.routes());
    app.use(logs.routes());
    app.use(user.routes());
    app.use(character.routes());
    app.use(records.routes());
    app.use(resetPassword.routes());
    app.use(postClassify.routes());
    app.use(profile.routes());
    app.use(dashboard.routes());
    app.use(datashow.routes());
    app.use(upload.routes());
    app.use(notices.routes());
    app.use(moderApply.routes());
    app.use(comment.routes());
    app.use(userScore.routes());
    app.use(message.routes());

};