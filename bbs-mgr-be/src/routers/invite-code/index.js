const Router = require('@koa/router');
const mongoose = require('mongoose');
const {getBody} = require('../../helpers/utils');
const InviteCode = mongoose.model('InviteCode');
const {
    v4: uuidv4
} = require('uuid');

const router = new Router({
    prefix: '/invite',
});

router.post('/add', async (ctx) => {

    let {
        count
    } = getBody(ctx);

    count = Number(count);
    console.log(count)

    const arr = [];
    for(let i = 0; i < count; i++){
        arr.push({
            code: uuidv4(),
            user: ''
        })
    }
    
    const res = await InviteCode.insertMany(arr);
    
    ctx.body = {
        code: 1,
        data: res,
        msg: '创建成功'
    }
})

router.get('/list', async (ctx) => {

    let {
        page,
        size,
    } = ctx.query;

    size = Number(size);
    page = Number(page);

    const list = await InviteCode
        .find()
        .sort({
            _id: -1
        })
        .skip((page - 1) * size)
        .limit(size)
        .exec();
    
    const total = await InviteCode.find().countDocuments().exec();

    ctx.body = {
        code: 1,
        data: {
            total,
            list,
            page,
            size
        },
        msg: '获取列表成功'
    }
})

router.delete('/:id', async (ctx) => {
    
    let {
       id
    } = ctx.params;

    const res = await InviteCode.deleteOne({
        _id: id
    }).exec();

    ctx.body = {
        code: 1,
        data: res,
        msg: '删除成功'
    }
})

module.exports = router;