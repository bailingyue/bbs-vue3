const Router = require('@koa/router');
const mongoose = require('mongoose');
// const {getBody} = require('../../helpers/utils');
const Logs = mongoose.model('Logs');


const router = new Router({
    prefix: '/logs',
});

router.get('/list', async (ctx) => {
    let {
        postId,
        page,
        size,
    } = ctx.query;

    size = Number(size);
    page = Number(page);

    const list = await Logs
        .find({
            postId
        })
        .sort({
            _id: -1
        })
        .skip((page - 1) * size)
        .limit(size)
        .exec();
    
    const total = await Logs.find({
        postId
    }).countDocuments().exec();

    ctx.body = {
        data: {
            total,
            list,
            page,
            size
        },
        msg: '获取列表成功',
        code: 1,
    };

});


module.exports = router;