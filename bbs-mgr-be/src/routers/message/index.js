const Router = require("@koa/router");
const mongoose = require("mongoose");
const {
    getBody
} = require("../../helpers/utils");
const Message = mongoose.model("Message");
const {
    verify,
    getToken
} = require("../../helpers/jwt");

const router = new Router({
    prefix: "/messages",
});

router.get("/like", async (ctx) => {
    const tokenInfo = await verify(getToken(ctx));

    const _filter = {
        //多字段匹配
        $and: [
         {ReceiveUser: tokenInfo.account},
         {
            $or:[
                {type:'post_like'},
                {type:'post_comment_like'},
            ]
         },
         {status: 1},
        ]
       }
    const like_list = await Message.find(_filter)
    .sort({
        _id: -1,
    })
    .exec();

    // console.log(like_list)


    const like_total = await Message.find(_filter).countDocuments().exec();
    

    ctx.body = {
        data: {
            like_total,
            like_list,
        },
        msg: "消息请求成功",
        code: 1,
    };
});

router.get("/comment", async (ctx) => {
    const tokenInfo = await verify(getToken(ctx));

    const comment_list = await Message.find({
        ReceiveUser: tokenInfo.account,
        type:'post_comment',
        status: 1
    })
    .sort({
        _id: -1,
    })
    .exec();


    const comment_total = await Message.find({
        ReceiveUser: tokenInfo.account,
        type:'post_comment',
        status: 1
    }).countDocuments().exec();

    ctx.body = {
        data: {
            comment_total,
            comment_list
        },
        msg: "消息请求成功",
        code: 1,
    };
});

router.get("/count", async (ctx) => {

    const tokenInfo = await verify(getToken(ctx));
    const _filter = {
        //多字段匹配
        $and: [
         {ReceiveUser: tokenInfo.account},
         {
            $or:[
                {type:'post_like'},
                {type:'post_comment_like'},
            ]
         },
         {status: 1},
        ]
       }

    const like_total = await Message.find(_filter).countDocuments().exec();
    const comment_total = await Message.find({
        ReceiveUser: tokenInfo.account,
        type:'post_comment',
        status: 1
    }).countDocuments().exec();

    ctx.body = {
        data: {
            like_total,
            comment_total
        },
        code: 1,
    };
});

router.post('/update/status', async (ctx) => {
    const {
        _id
    } = getBody(ctx);

    const one = await Message.findOne({
        _id
    }).exec();

    if(!one){
        ctx.body = {
            msg: '该消息不存在',
            code: 0
        }
        return;
    }

    one.status = 0;
    await one.save();

    ctx.body = {
        code: 1,
    }

});


module.exports = router;