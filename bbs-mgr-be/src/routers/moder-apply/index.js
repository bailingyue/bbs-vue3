const Router = require('@koa/router');
const mongoose = require('mongoose');
const {getBody} = require('../../helpers/utils');
const ModerApply = mongoose.model('ModerApply');
const User = mongoose.model('User');
const config = require('../../project.config');
const PostClassify = mongoose.model("PostClassify");
const Character = mongoose.model("Character");

const router = new Router({
    prefix: '/moder-apply',
});

router.get('/list', async (ctx) => {
    let {
        page,
        size,
    } = ctx.query;

    size = Number(size);
    page = Number(page);

    const list = await ModerApply
        .find({
            status: 1
        })
        .sort({
            _id: -1
        })
        .skip((page - 1) * size)
        .limit(size)
        .exec();
    
    const total = await ModerApply.find({
        status: 1
    }).countDocuments().exec();

    ctx.body = {
        data: {
            total,
            list,
            page,
            size
        },
        msg: '获取列表成功',
        code: 1,
    };

});

router.post('/update/status', async (ctx) => {
    const {
        id,
        account,
        classify
    } = getBody(ctx);

    let {
        status
    } = getBody(ctx);

    status = Number(status);

    // 校验
    const one = await ModerApply.findOne({
        _id: id
    }).exec();
    if(!one){
        ctx.body = {
            msg: '找不到这条申请',
            code: 0
        }
        return;
    }

    const user = await User.findOne({
        account
    }).exec();
    if(!user){
        ctx.body = {
            msg: '该用户不存在',
            code: 0
        }
        return;
    }

    const classifyOne = await PostClassify.findOne({
        _id: classify
    })
    if(!classifyOne){
        ctx.body = {
            msg: '该分类不存在',
            code: 0
        }
        return;
    }

    // 找到版主对应的characterID
    const morder = await Character.findOne({
        name: "moderator"
    })

    // 如果同意 则置为该板块的版主 user自身character classify改变
    if(status == 2){
        user.classify =  classify;
        user.character =  morder._id;
        await user.save();
    }

    one.status = status;
    await one.save();

    ctx.body = {
        msg: '处理成功',
        code: 1
    }

});
module.exports = router;
