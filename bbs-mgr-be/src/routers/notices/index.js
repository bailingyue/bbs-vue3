const Router = require('@koa/router');
const mongoose = require('mongoose');
const {getBody} = require('../../helpers/utils');
const Notice = mongoose.model('Notice');
const PostClassify = mongoose.model('PostClassify');
const config = require('../../project.config');
const User = mongoose.model('User');
const {loadExcel,getFirstSheet} = require('../../helpers/excel')
const {verify, getToken} = require('../../helpers/jwt');

const router = new Router({
    prefix: '/notices',
});

// 处理前台公告 列表置顶
const showNotice = async (status = 0, classify = '') => {
    const showJudge = {
        $and: [
            {status},
            {classify}
        ]
    }

    const list = await Notice
    .find(showJudge)
    .sort({
        _id: -1
    })
    .exec();

    return list;
}

// 主要是针对公告管理 
router.get('/listAll', async (ctx) => {
     // url 请求
     let {
        // page,
        // size,
        keyword
    } = ctx.query;

    // size = Number(size);
    // page = Number(page);

    const searchF = {
        $and: [
            {status: 1},
        ] 
    };
    const searchB = {
        $and: [
            {status: 0},
        ] 
    };

    if (keyword) {
        //正则匹配 i忽略大小写
        let reg = new RegExp(keyword, "i");
        var _filter = {
            //多字段匹配
            $or: [
              {title: {$regex: reg}},
              {author: {$regex: reg}},
              {classify: {$regex: reg}},
            ]
        }
        searchF['$and'].push(_filter);
        searchB['$and'].push(_filter);
    }

    const list_forward = await Notice
        .find(searchF)
        .sort({
            _id: -1
        })
        .exec();
    

    const list_backward = await Notice
        .find(searchB)
        .sort({
            _id: -1
        })
        .exec();

    const list = list_forward.concat(list_backward);

    // if(keyword){
    //     var total = await Notice.countDocuments(_filter).exec();
    // }else{
    //     var total = await Notice.countDocuments().exec();
    // }

    ctx.body = {
        data: list,
        code: 1,
        msg: '获取列表成功',
    }
    // total = null;
    _filter = null;
});

// 主要针对前端 公告、板块展示需要的
router.get('/list', async (ctx) => { 
    const {
        classify = "",
    } = ctx.query;

    const list_F = await showNotice(1, classify);
    const list_B = await showNotice(0, classify);
    const list = list_F.concat(list_B);


    ctx.body = {
        data: list,
        msg: '获取公告成功',
        code: 1,
    };

});

router.post('/add', async (ctx) => {
    const {
        title,
        content,
        classify = "",
        isManage
    } = getBody(ctx);

    let{
        author
    }= getBody(ctx);
    
    console.log({
        title,
        content,
        author,
        classify,
        isManage
    })
        
    if(!title || !content){
        return;
    }
  
    if(!isManage){
        // 首页
        const res = await verify(getToken(ctx));
        if(!res){
            ctx.body = {
                code: 0,
                msg: '访问非法',
                data: null
            }
            return;
        }
        author = res.account;
    }else{
        // 管理台
        if(!author){
            ctx.body = {
                code: 0,
                msg: '用户不为空',
            };
            return;
        }
        
        const findUser = await User.findOne({
            account: author
        }).exec();
        if(!findUser){
            ctx.body = {
                data: null,
                code: 0,
                msg: '用户不存在',
            };
            return;
        }
    }

    const findNotice = await Notice.findOne({
        title,
        author,
        classify
    }).exec();
    if(findNotice){
        ctx.body = {
            code: 0,
            msg: '已存在该公告',
            data: null
        }
        return;
    }

    // console.log(author)

    const notice = new Notice({
        title,
        author,
        classify,
        content,
        status: 0
    })
    const resN = await notice.save();
    console.log(resN)

    ctx.body = {
        data: resN,
        code: 1,
        msg: '发布成功',
    };
});

router.post('/update', async (ctx) => {
    const {
        id,
        ...others
    } = getBody(ctx);
    const token = await verify(getToken(ctx));
    
    const author = token.account;

    const findNotice = await Notice.findOne({
        _id: id
    }).exec();
    if(!findNotice){
        ctx.body = {
            code: 0,
            msg: '找不到公告',
            data: null
        }
        return;
    }

    const newQuery = {
        author
    };
    Object.entries(others).forEach(([key, value]) => {
        if (value) {
            newQuery[key] = value;
        }
    });

    if(newQuery['author']){
        const findUser = await User.findOne({
            account: newQuery['author']
        })
        if(!findUser){
            ctx.body = {
                data: null,
                msg: '不存在该用户',
                code: 0,
            };
            return;
        }
    }

    Object.assign(findNotice, newQuery);
    const res = await findNotice.save();

    ctx.body = {
        msg: '更新成功',
        code: 1,
        data: res
    };
});

router.delete('/:id', async (ctx) => {
    const {
        id,
    } = ctx.params;

    const delMsg = await Notice.deleteOne({
        _id: id
    });

    ctx.body = {
        data: delMsg,
        msg: '删除成功',
        code: 1,
    };


})

router.post('/update/status', async (ctx) => {
    const {
        id
    } = getBody(ctx);

    let {
        status
    } = getBody(ctx);

    status = Number(status);

    const one = await Notice.findOne({
        _id: id
    }).exec();

    if(!one){
        ctx.body = {
            msg: '帖子不存在',
            code: 0
        }
        return;
    }

    one.status = status;
    await one.save();

    let mes = '';
    if(status){
        mes = '置顶成功'
    }else{
        mes = '取消置顶'
    }

    ctx.body = {
        msg: mes,
        code: 1,
    }

});

router.post('/addMany', async(ctx) => {
    const {
        Key
    } = getBody(ctx);

    console.log(Key)

    if(!Key){
        return;
    }
    
    const path = `${config.UPLOAD_DIR}/${Key}`;
    const excel = loadExcel(path);
    const sheet = getFirstSheet(excel);

    const arr = [];
    // forEach 使用异步 会造成变量隔绝
    // 解决 for of
    console.log(sheet)
    for(item of sheet){
        let [
            name,
            author,
            content,
            title = "未分类"
        ] = item;
        console.log(title)

        if(!name || !author || !content){
            return;
        }

        const findUser = await User.findOne({
            account:author
        }).exec();
        if(!findUser){
            continue;
        }
 
        const classify = await PostClassify.findOne({
            title
        }).exec();
        if(!classify){
            continue;
        }

        const findOne = await Notice.findOne({
            title: name,
            author,
            classify: classify._id
        }).exec();
        if(findOne){
            continue;
        }

        arr.push({
            title,
            author,
            classify: classify._id,
            content,
            status: 0
        })
    }

    await Notice.insertMany(arr);
    ctx.body = {
        data: null,
        code: 1,
        msg: '添加成功'
    }
})
module.exports = router;