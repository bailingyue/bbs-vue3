const Router = require('@koa/router');
const mongoose = require('mongoose');
const {getBody} = require('../../helpers/utils');
const PostClassify = mongoose.model('PostClassify');
const User = mongoose.model('User');
const Character = mongoose.model('Character');
const Post = mongoose.model('Post');
const Comment = mongoose.model('Comment');
const Notice = mongoose.model('Notice');
const CommentLike = mongoose.model('CommentLike');
const PostLike = mongoose.model('PostLike');
const Message = mongoose.model("Message");

const router = new Router({
    prefix: '/post-classify',
});

// 注意此处返回对象
const getModerator = async (classify = '') => {
    const moderChar = await Character.findOne({
        name: "moderator"
    }).exec();

    const moderList = await User.find({
        character: moderChar._id,
        classify
    })
    .sort({
        _id: -1
    }).exec();

    return moderList
}

router.get('/listAll', async (ctx) => {

    const list = await PostClassify.find().exec();
    ctx.body = {
        data: list,
        code: 1,
        msg: '获取列表成功',
    }
});

router.post('/add', async (ctx) => {
    const {
        title,
    } = getBody(ctx);
    if(!title){
        return;
    }

    const one = await PostClassify.findOne({
        title
    }).exec();

    if(one){
        ctx.body = {
            code: 0,
            msg: '已存在该类型',
            data: null
        }
        return;
    }

    const classifies = new PostClassify({
        title
    })

    const res = await classifies.save();

    ctx.body = {
        data: res,
        code: 1,
        msg: '添加成功',
    };
});

router.get('/list', async (ctx) => {
    // url 请求
    let {
        page,
        size,
    } = ctx.query;

    size = Number(size);
    page = Number(page);

    const list = await PostClassify
        .find()
        .sort({
            _id: -1
        })
        .skip((page - 1) * size)
        .limit(size)
        .exec();

    const total = await PostClassify.countDocuments().exec();

    ctx.body = {
        data: {
            total,
            list,
        },
        code: 1,
        msg: '获取列表成功',
    }
});

router.post('/delete', async (ctx) => {
    const {
        id,
        moderators
    } = getBody(ctx);

    console.log( {
        id,
        moderators
    })

    const one = await PostClassify.findOne({
        _id: id
    });
    if(!one){
        ctx.body = {
            data: null,
            msg: `出错了`,
            code: 0,
        };
        return;
    }

    for(let item of moderators){
        const user = await User.findOne({
            account: item
        }).exec();
        if(!user){
            ctx.body = {
                data: null,
                msg: `出错了`,
                code: 0,
            };
            return;
        }
        user.classify = '';
        await user.save();
    }

    await Notice.deleteMany({
        classify: id
    })

    const posts = await Post.find({
        classify: id
    });

    for(let item of posts){
        
        await Post.deleteOne({
            _id: item._id
        });
    
        const comment = await Comment.find({
            postId: item._id
        })
        for(let comm of comment){
            const one = await CommentLike.findOne({
             commentId: comm._id
            });
     
            if(!one){
                 ctx.body = {
                     msg: '不存在',
                     code: 0,
                 };
                 return;
            }
            await CommentLike.deleteOne({
             commentId: one.commentId
            });
         }
    
        await Comment.deleteMany({
            postId: item._id
        })
       
        await PostLike.deleteMany({
            postId:  item._id
        })
    }

    const delMsg = await PostClassify.deleteOne({
        _id: id
    });

    await Message.deleteMany({
        classify: id
    })

    ctx.body = {
        data: delMsg,
        msg: '删除成功',
        code: 1,
    };

})

router.post('/update', async (ctx) => {
    const {
        id,
        title,
        content = '',
        moderators
    } = getBody(ctx);

    if(!title){return;}

    const one = await PostClassify.findOne({
        _id: id
    }).exec();

    if (!one) {
        ctx.body = {
            data: null,
            msg: '没有找到类型',
            code: 0,
        };
        return;
    }
    // 如果加入找不到原先 则删除原先 若加入在原先中 则删除加入
    for(let item of one.moderators){
        let index = moderators.indexOf(item);
        if( index == -1){
            one.moderators.remove(item);
            // 需要更新该删除角色的classify 即置为''
            const user = await User.findOne({
                account: item
            }).exec();
            if(!user){
                ctx.body = {
                    data: null,
                    msg: `出错了`,
                    code: 0,
                };
                return;
            }
            user.classify = '';
            await user.save();

        }else{
            moderators.splice(index, 1);
        }
    }

    // 校验剩余有效性
    const freeModer = await getModerator();
    let freeList = [];
    for(let item of freeModer){
        freeList.push(item.account)
    }
    for(let item of moderators){
        let index = freeList.indexOf(item);
        if( index == -1){
             ctx.body = {
                data: null,
                msg: `${item}版主不存在, 请重新选择`,
                code: 0,
            };
            return;
        }
    }

    one.title = title;
    one.content = content;
    one.moderators =  one.moderators.concat(moderators);

    // 更新版主信息 one.moderators
    for(let item of one.moderators){
        const user = await User.findOne({
            account: item
        }).exec();
        
        // 如果没有 添加
        if(user.classify != id){
            user.classify = id
            await user.save();
        }
    }

    const res = await one.save();

    ctx.body = {
        msg: '更新成功',
        code: 1,
    };
})

router.get('/moderator-list', async (ctx) => {

    ctx.body = {
        msg: '',
        code: 1,
        data: await getModerator()
    };

});



module.exports = router;
