const Router = require('@koa/router');
const mongoose = require('mongoose');
const {
    getBody
} = require('../../helpers/utils');
const config = require('../../project.config');
const {loadExcel,getFirstSheet} = require('../../helpers/excel')
const Post = mongoose.model('Post');
const Logs = mongoose.model('Logs');
const User = mongoose.model('User');
const PostClassify = mongoose.model('PostClassify');
const Comment = mongoose.model('Comment');
const CommentLike = mongoose.model('CommentLike');
const PostLike = mongoose.model('PostLike');
const UserScore = mongoose.model("UserScore");
const Character = mongoose.model("Character");
const Message = mongoose.model("Message");


const {verify, getToken} = require('../../helpers/jwt');

const getUserScoreObject = async (ctx, account) => {
    // 查询数据是否存在
    const fineuser = await User.findOne({
        account
    })
    if(!fineuser){
        ctx.body = {
            code: 0,
            msg: '用户不存在',
        }
        return;
    }

      const fineOne = await UserScore.findOne({
          userId: fineuser._Id
      })
      if(!fineOne){
          const res = new UserScore ({
              userId,
              score: 0
          })
  
          await res.save();   
          ctx.body = {
              data: res,
              code: 1
          }
          return;
      }
  
      return fineOne;
}

const showPost = async (status = 0, classify, bySort, _filter) => {
    if(!bySort){

        bySort = {
            _id: -1
        }
    }else{

        bySort = {
            likes: -1
        }
    }
    let tempAnd = [];
    if(classify){
        tempAnd = [
            {status},
            {classify}
        ]
    }else{
        tempAnd = [
            {status}
        ]
    }
    const showJudge = {
        $and: tempAnd
    }

    if(_filter){
        showJudge['$and'].push(_filter)
    }

    const list = await Post
    .find(showJudge)
    .sort(bySort)
    .exec();

    return list;

}

const findPostOne = async (id) => {
    const one = await Post.findOne({
        _id:id
    }).exec();
    return one;
};

const router = new Router({
    prefix: '/post',
});

router.post('/add', async (ctx) => {
    const {
        name,
        author,
        date,
        classify,
        content
    } = getBody(ctx);
    if(!name || !author || !date || !classify || !content){
        ctx.body = {
            code: 0,
            msg: '内容不能为空',
            data: null
        }
        return;
    }

    const findPost = await Post.findOne({
        name,
        author
    }).exec();

    if(findPost){
        ctx.body = {
            code: 0,
            msg: '已存在该帖子',
            data: null
        }
        return;
    }

    const findUser = await User.findOne({
        account:author
    }).exec();
    

    if(!findUser){
        ctx.body = {
            code: 0,
            msg: '该用户不存在',
            data: null
        }
        return;
    }

    const posts = new Post({
        name,
        author,
        date,
        classify,
        content,
        comments: 0,
        likes: 0,
        status: 0,
        isAdd: 0
    })

    const res = await posts.save();

    ctx.body = {
        data: res,
        code: 1,
        msg: '添加成功',
    };
});

// 前台
router.get('/list', async (ctx) => {
    // url 请求
    let {
        keyword,
        classify,
        bySort
    } = ctx.query;

    if (keyword) {
        //正则匹配 i忽略大小写
        let reg = new RegExp(keyword, "i");
        var _filter = {
            //多字段匹配
            $or: [
              {name: {$regex: reg}},
              {author: {$regex: reg}},
              {classify: {$regex: reg}},
            ]
        }
    }

    // 经研究 必须深度拷贝 否则合并出来的对象为只读属性
    const list_F = JSON.parse(JSON.stringify(await showPost(1, classify, bySort, _filter)));
    const list_B = JSON.parse(JSON.stringify(await showPost(0, classify, bySort, _filter)));

    const list = list_F.concat(list_B);
    // console.log(list)

    // 前台渲染 点赞状态
    const user = await verify(getToken(ctx));
    if(!user){
        ctx.body = {
            code: 0,
            msg: '访问非法',
            data: null
        }
        return;
    }

    // for of 等无法修改原数组的值 
    for(let i = 0; i < list.length;  i++){
        // 找到每个帖子找到对应状态
        const one = await PostLike.findOne({
            postId: list[i]._id
        })
        // 如果没有 则无人点赞 isLike置为0
        if(!one){
            list[i].isLike = 0
            continue;
        }

        // 有 循环判断user是否点赞
        const index = await one.userList.findIndex(itemId => itemId == user._id);
        if(index != -1){
            list[i].isLike = 1
        }else{
            list[i].isLike = 0
        }
    }

    // 循环整个list 遇到isAdd=0且likes>xx 则score+10 并将isAdd置为1
    for(let i = 0; i < list.length; i++){
        if(list[i].likes > config.SCORE_LIKE && !list[i].isAdd){
            const scoreObject = await getUserScoreObject(ctx, list[i].account);
            scoreObject.score += config.SCORE_LIKE;
            await scoreObject.save();

            list[i].isAdd = 1;
        }
    }

    ctx.body = {
        data: list,
        code: 1,
        msg: '获取列表成功',
    }
    _filter = null;
});

// 管理台
router.get('/listAll', async (ctx) => {
    // url 请求
    let {
        keyword
    } = ctx.query;

    const searchF = {
        $and: [
            {status: 1},
        ] 
    };
    const searchB = {
        $and: [
            {status: 0},
        ] 
    };
    
    if (keyword) {
        //正则匹配 i忽略大小写
        let reg = new RegExp(keyword, "i");
        var _filter = {
            //多字段匹配
            $or: [
              {name: {$regex: reg}},
              {author: {$regex: reg}},
              {classify: {$regex: reg}},
            ]
        }
        searchF['$and'].push(_filter);
        searchB['$and'].push(_filter);
    }

    const list_forward = await Post
        .find(searchF)
        .sort({
            _id: -1
        })
        .exec();

    const list_backward = await Post
        .find(searchB)
        .sort({
            _id: -1
        })
        .exec();

    const list = list_forward.concat(list_backward);

    ctx.body = {
        data: list,
        code: 1,
        msg: '获取列表成功',
    }
    _filter = null;
});

router.delete('/:id', async (ctx) => {
    const {
        id,
    } = ctx.params;

    const delMsg = await Post.deleteOne({
        _id: id
    });
    

    const comment = await Comment.find({
        postId: id
    })

    for(let comm of comment){
       const one = await CommentLike.findOne({
        commentId: comm._id
       });
       
       if(!one){
            ctx.body = {
                msg: '不存在',
                code: 0,
            };
            return;
       }
       await CommentLike.deleteOne({
        commentId: one.commentId
       });
    }

    await Comment.deleteMany({
        postId: id
    })  
   
    await PostLike.deleteMany({
        postId: id
    })

    // 删除帖子点赞、回复，评论点赞消息
    await Message.deleteMany({
        postId: id,
    })

    ctx.body = {
        data: delMsg,
        msg: '删除成功',
        code: 1,
    };


})

// 改变点赞状态
router.post('/update', async (ctx) => {
    const {
        id,
        // 表示将要置为的状态
        furStatus,
        isForward,
        ...others
    } = getBody(ctx);

    // 校验
    const findPost = await Post.findOne({
        _id: id
    }).exec();
    if(!findPost){
        ctx.body = {
            code: 0,
            msg: '帖子不存在',
            data: null
        }
        return;
    }
    const user = await verify(getToken(ctx));
    if(!user){
        ctx.body = {
            code: 0,
            msg: '访问非法',
            data: null
        }
        return;
    }
    
    const userChar = await Character.findOne({
        _id: user.character
    })
    if(!userChar){return;}

    // 如果是前台 校验修改的帖子的author与上传过来的是否一致
    if(isForward && userChar.name != 'admin'){
        // 如果是 则可以修改 否则不可修改
        if(findPost.author != user.account){
            ctx.body = {
                data: null,
                msg: '不是您发布的帖子，无法修改。',
                code: 0
            };
            return;
        }
    }

    // 点赞逻辑
    if(typeof furStatus != 'undefined'){

        const one = await PostLike.findOne({
            postId: id
        }).exec();
        // 已有人点赞 user将要点赞
        if(one && furStatus){
            // 查找user是否点赞 这里存放userId
            const index = await one.userList.findIndex(itemId => itemId == user._id);
            console.log("111111111111111")
            // console.log(index)
            // user已点赞
            if(index != -1){
                ctx.body = {
                    code: 0,
                    msg: '您已点赞'
                }
                return;
            }
            // user未点赞
            one.userList.push(user._id);
            await one.save();
            findPost.likes += 1;
            await findPost.save();
            status = 1;

        // 已有人点赞 user将取消点赞
        }else if(one && !furStatus){
            console.log("2222222222222")

            const index = await one.userList.findIndex(itemId => itemId == user._id);
            // user未点赞
            if(index == -1){
                ctx.body = {
                    code: 0,
                    msg: '您尚未点赞'
                }
                return;
            }
            // user已点赞
            one.userList.splice(index, 1);
            await one.save();
            findPost.likes = findPost.likes - 1 > 0 ? findPost.likes -= 1 : 0;
            await findPost.save();
            status = 0;
        }
        // 无人点赞
        if(!one){
            console.log("333333333333")

            // user将要点赞
            if(furStatus){
                const userList = [];
                userList.push(user._id);
                const res = new PostLike ({
                    postId: id,
                    userList
                })
                await res.save();
                findPost.likes += 1;
                await findPost.save();
                status = 1;
            }else{
            // user将取消点赞
                ctx.body = {
                    code: 0,
                    msg: '出错了'
                }
                return;
            }
        }
    }

    const newQuery = {};
    Object.entries(others).forEach(([key, value]) => {
        if (value) {
            newQuery[key] = value;
        }
    });
    if(newQuery['author']){
        const findUser = await User.findOne({
            account: newQuery['author']
        })
        if(!findUser){
            ctx.body = {
                data: null,
                msg: '不存在该用户',
                code: 0,
            };
            return;
        }
    }
    Object.assign(findPost, newQuery);
    const res = await findPost.save();

    if(isForward){
        // 消息逻辑
        const findMessage = await Message.findOne({
            type: 'post_like',
            postId: id,
            sendUser: user.account,
            ReceiveUser: findPost.author,
        })
        if(!findMessage){
            const res = new Message({
                type: 'post_like',
                postId: id,
                title: findPost.name,
                sendUser: user.account,
                ReceiveUser: findPost.author,
                content: findPost.content,
                classify: findPost.classify,
                status,
            });
            await res.save();
        }else{
            findMessage.status = status;
            await findMessage.save();
        }
    }else{
        // 加入logs日志
        const log = new Logs({
            postId: id,
            content: newQuery.content,
            classify: newQuery.classify || findPost.classify,
            account: user.account
            // type
        });
        await log.save()
    }

    ctx.body = {
        data: res,
        msg: '更新成功',
        code: 1,
    };
})

router.get('/detail/:id', async(ctx) => {
    const {
        id,
    } = ctx.params;
    const one = await findPostOne(id);
    

    ctx.body = {
        msg: '查询成功',
        code: 1,
        data: one
    };

});

router.post('/addMany', async(ctx) => {
    const {
        Key
    } = getBody(ctx);

    console.log(Key)

    if(!Key){
        return;
    }
    
    
    const path = `${config.UPLOAD_DIR}/${Key}`;
    const excel = loadExcel(path);
    const sheet = getFirstSheet(excel);

    const arr = [];
    // forEach 使用异步 会造成变量隔绝
    // 解决 for of

    console.log(sheet)

    const timestamp = (new Date()).valueOf();

    for(item of sheet){
        let [
            name,
            author,
            date,
            title = '未分类',
            content
        ] = item;

        if(!author || !name || !content){
            ctx.body = {
                data: null,
                code: 0,
                msg: '添加失败, 名称、作者和内容不能为空'
            }
            return;
        }

        const findUser = await User.findOne({
            account:author
        }).exec();
        if(!findUser){
            continue;
        }

        const classify = await PostClassify.findOne({
            title
        }).exec();
        if(!classify){
            continue;
        }

        const findOne = await Post.findOne({
            name,
            author
        }).exec();
        if(findOne){
            continue;
        }
        
        arr.push({
            name,
            author,
            date: timestamp || date.valueOf() ,
            classify: classify._id,
            content,
            browses: 0,
            comments: 0,
            likes: 0,
            status: 0
        })
       
    }

    await Post.insertMany(arr);
    ctx.body = {
        data: null,
        code: 1,
        msg: '添加成功'
    }
})

router.post('/update/status', async (ctx) => {
    const {
        id
    } = getBody(ctx);

    let {
        status
    } = getBody(ctx);

    status = Number(status);

    const one = await Post.findOne({
        _id: id
    }).exec();

    if(!one){
        ctx.body = {
            msg: '帖子不存在',
            code: 0
        }
        return;
    }

    // 如果将置顶的帖子未曾得分 则置为1 且不变
    if(!one.isAdd){
        // 找到发帖用户
        const user = await User.findOne({
            account: one.author
        }).exec();

        const userScore = await UserScore.findOne({
            userId: user._id
        }).exec();

        userScore.score += config.SCORE_TOP;
        await userScore.save();
        // console.log(userScore)

        one.isAdd = 1;
        await one.save();
    }

    one.status = status;
    await one.save();

    let mes = '';
    if(status){
        mes = '置顶成功'
    }else{
        mes = '取消置顶'
    }

    ctx.body = {
        msg: mes,
        code: 1,
    }

});
module.exports = router;