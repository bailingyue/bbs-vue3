const Router = require("@koa/router");
const mongoose = require("mongoose");
const {getBody} = require('../../helpers/utils');
const config = require('../../project.config');
const User = mongoose.model("User");
const PostClassify = mongoose.model("PostClassify");
const Character = mongoose.model("Character");
const ModerApply = mongoose.model("ModerApply");
const UserScore = mongoose.model("UserScore");


const {verify, getToken} = require('../../helpers/jwt');

const router = new Router({
    prefix: "/profile",
});

const getUserScore = async (ctx) => {
  // 查询数据是否存在
  const user = await verify(getToken(ctx));
        if(!user){
            ctx.body = {
                code: 0,
                msg: '访问非法',
                data: null
            }
            return;
        }
    const userId = user._id;
    const fineOne = await UserScore.findOne({
        userId
    })
    if(!fineOne){
        const res = new UserScore ({
            userId,
            score: 0
        })

        await res.save();   
        ctx.body = {
            data: res,
            code: 1
        }
        return;
    }

    return fineOne.score;
}

const verifyToken =  async (ctx) => {
    const { _id } = await verify(getToken(ctx));
    const user = await User.findOne({
        _id
    })
    if(!user){
        ctx.body = {
            data: null,
            msg: '用户不存在',
            code: 0,
        };
        return;
    }
    return user;
}

router.post("/update/password", async (ctx) => {
    const {
        newPassword,
        oldPassword
    } = getBody(ctx);

    const user = await verifyToken(ctx);

    if(user.password != oldPassword){
        ctx.body = {
            data: null,
            msg: '密码校验失败',
            code: 0,
        };
        return;
    }

    user.password = newPassword;
    const res = await user.save();

    ctx.body = {
        data: res,
        msg: '密码修改成功',
        code: 1,
    };
})

router.post("/update/apply", async (ctx) => {
    const {
        classify,
        content
    } = getBody(ctx);
    console.log(classify, content)
    if(!classify || !content) return;
    const userScore = await getUserScore(ctx);
    if(userScore < config.APPLY_MODERATER_SCORE){
        ctx.body = {
            msg: `您的积分是：${userScore}分，需要满足${config.APPLY_MODERATER_SCORE}分才可以申请版主。`,
            code: 0,
        };
        return;
    }

    const user = await verifyToken(ctx);
    const userCharacter = await Character.findOne({
        _id: user.character
    })
    if(userCharacter.name != "member"){
        ctx.body = {
            data: null,
            msg: `您已是${userCharacter.name}, 无需申请。`,
            code: 0,
        };
        return;
    }

    const classifyOne = await PostClassify.findOne({
        _id: classify
    })
    if(!classifyOne){
        ctx.body = {
            data: null,
            msg: '出错了',
            code: 0,
        };
    }

    const applyOne = await ModerApply.findOne({
        account: user.account,
    })
    if(applyOne){
        ctx.body = {
            data: null,
            msg: '你已经申请过了',
            code: 0,
        };
        return;
    }

    const one = new ModerApply({
        account: user.account,
        classify,
        content,
        // 1 待处理
        // 2 同意
        // 3 拒绝

        status: 1,
    })

    const res = await one.save();

    ctx.body = {
        data: res,
        msg: '申请成功',
        code: 1,
    };
})
module.exports = router;

