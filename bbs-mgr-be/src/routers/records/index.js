const Router = require('@koa/router');
const mongoose = require('mongoose');
const {getBody} = require('../../helpers/utils');
const Records = mongoose.model('Records');


const router = new Router({
    prefix: '/records',
});

router.get('/list', async (ctx) => {
    let {
        page = 1,
        size = 10,
    } = ctx.query;

    size = Number(size);
    page = Number(page);

    const list = await Records
        .find({
            show: true
        })
        .sort({
            _id: -1
        })
        .skip((page - 1) * size)
        .limit(size)
        .exec();
    
    const total = await Records.find({
        show: true
    }).countDocuments().exec();

    ctx.body = {
        data: {
            total,
            list,
            page,
            size
        },
        msg: '获取列表成功',
        code: 1,
    };

});

router.post('/delete', async (ctx) => {
    const {
        id
    } = getBody(ctx);

    const res =  await Records.findOne({
        _id: id
    }).exec();

    if(!one){
        ctx.body = {
            code: 1,
            msg: '删除成功',
            data: {}
        }
        return;
    }

    one.show = false;
    await one.save();

    ctx.body = {
        code: 1,
        msg: '删除成功',
        data: res
    }

})

module.exports = router;