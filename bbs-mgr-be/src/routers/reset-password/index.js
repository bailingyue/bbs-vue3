const Router = require('@koa/router');
const mongoose = require('mongoose');
const {getBody} = require('../../helpers/utils');
const ResetPassword = mongoose.model('ResetPassword');
const User = mongoose.model('User');
const config = require('../../project.config');

const router = new Router({
    prefix: '/reset-password',
});

router.get('/list', async (ctx) => {
    let {
        page,
        size,
    } = ctx.query;

    size = Number(size);
    page = Number(page);

    const list = await ResetPassword
        .find({
            status: 1
        })
        .sort({
            _id: -1
        })
        .skip((page - 1) * size)
        .limit(size)
        .exec();
    
    const total = await ResetPassword.find({
        status: 1
    }).countDocuments().exec();

    ctx.body = {
        data: {
            total,
            list,
            page,
            size
        },
        msg: '获取列表成功',
        code: 1,
    };

});

router.post('/add', async (ctx) => {
    // 账户不存在
    const {
       account
    } = getBody(ctx);

    const findUser = await User.findOne({
        account
    }).exec();

    // 防止恶意申请 拦截处理
    if(!findUser){
        ctx.body = {
            msg: '申请成功了',
            code: 1,
        };
        return;
    }

    // 在 forget-password 集合中不存在 status为1 的文档
    const one = await ResetPassword.findOne({
        account,
        status: 1
    }).exec();

    if(one){
        ctx.body = {
            msg: '已经申请过了',
            code: 1,
        };
        return;
    }

    const forgetPwd = new ResetPassword({
        account,
        status: 1
    })

    await forgetPwd.save();

    ctx.body = {
        msg: '申请成功了',
        code: 1,
    };

});

router.post('/update/status', async (ctx) => {
    const {
        id,
        account
    } = getBody(ctx);

    let {
        status
    } = getBody(ctx);

    status = Number(status);

    const one = await ResetPassword.findOne({
        _id: id
    }).exec();

    if(!one){
        ctx.body = {
            msg: '找不到这条申请',
            code: 0
        }
        return;
    }

    if(status == 2){
        const user = await User.findOne({
            account
        }).exec();

        if(user){
            user.password =  config.DEFAULT_PASSWORD;
            await user.save();
        }else{   
            ctx.body = {
                msg: '该用户不存在',
                code: 0
            }
            return;
        }
    }

    one.status = status;
    await one.save();

    ctx.body = {
        msg: '处理成功',
        code: 1
    }

});
module.exports = router;
