const Router = require('@koa/router');
const mongoose = require('mongoose');
const {getBody} = require('../../helpers/utils');
// const Records = mongoose.model('Records');
const {saveFileToDisk, getUploadFileExt} = require('../../helpers/upload');
const config = require('../../project.config');
const path = require('path');
const {
    v4: uuidv4
} = require('uuid');

const router = new Router({
    prefix: '/upload',
});

router.post('/file', async (ctx) => {
    const ext = getUploadFileExt(ctx);
    if(ext != 'xlsx'){
        ctx.body = {
            data: null,
            msg: '只能上传excel文件',
            code: 0
        }
        return;
    }
    const filename = `${uuidv4()}.${ext}`;
    await saveFileToDisk(
        ctx, path.resolve(config.UPLOAD_DIR, filename)
    );
    ctx.body = {
        data: filename,
        msg: '文件上传成功',
        code: 1
    }
})

router.post('/pic', async (ctx) => {
    const ext = getUploadFileExt(ctx);
    const filename = `${uuidv4()}.${ext}`;
    let pic_list = ['png', 'jpg', 'psd', 'gif', 'bmp', 'svg', 'ai'];
    if(pic_list.includes(ext)){
        await saveFileToDisk(
            ctx, path.resolve(config.IMG_DIR, filename)
        );
        ctx.body = {
            data: filename,
            msg: '图片上传成功',
            code: 1
        }
    }else{
        ctx.body = {
            data: null,
            msg: '暂不支持该类型的图片',
            code: 0
        }
    }
  
})

module.exports = router;