const Router = require("@koa/router");
const mongoose = require("mongoose");
const { getBody } = require("../../helpers/utils");
const UserScore = mongoose.model("UserScore");
const { verify, getToken } = require("../../helpers/jwt");

const router = new Router({
  prefix: "/user-score",
});

router.get("/score", async (ctx) => {
  // 查询数据是否存在
  const user = await verify(getToken(ctx));
        if(!user){
            ctx.body = {
                code: 0,
                msg: '访问非法',
                data: null
            }
            return;
        }
    const userId = user._id;
    const fineOne = await UserScore.findOne({
        userId
    })
    if(!fineOne){
        const res = new UserScore ({
            userId,
            score: 0
        })

        await res.save();   
        ctx.body = {
            data: res,
            code: 1
        }
        return;
    }
    
    ctx.body = {
        data: fineOne,
        code: 1
    }

});


module.exports = router;
