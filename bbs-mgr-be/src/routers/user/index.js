const Router = require("@koa/router");
const mongoose = require("mongoose");
const {
  getBody
} = require("../../helpers/utils");
const config = require("../../project.config");
const User = mongoose.model("User");
const Character = mongoose.model("Character");
const PostClassify = mongoose.model("PostClassify");
const md5 = require("md5");

const {
  verify,
  getToken
} = require("../../helpers/jwt");

const {
  loadExcel,
  getFirstSheet
} = require("../../helpers/excel");

const router = new Router({
  prefix: "/user",
});

const changeCharacter = async (account, classify = "", oldclassify = "") => {
  if (classify == oldclassify && classify) {
    return;
  }

  if (oldclassify) {
    const one = await PostClassify.findOne({
      _id: oldclassify,
    });
    if (!one) return;
    const index = one.moderators.indexOf(account);
    if (index != -1) {
      one.moderators.splice(index, 1);
      const res = await one.save();
      console.log("old", res);
    }
  }

  if (classify) {
    const one = await PostClassify.findOne({
      _id: classify,
    });
    if (!one) return;
    const index = one.moderators.indexOf(account);
    if (index == -1) {
      one.moderators.push(account);
      const res = await one.save();
      console.log("new", res);
    }
  }
};

router.get("/list", async (ctx) => {
  let {
    page,
    size,
    keyword
  } = ctx.query;

  page = Number(page);
  size = Number(size);

  if (keyword) {
    const charOne = await Character.findOne({
      title: keyword,
    });
    //正则匹配 i忽略大小写
    let reg = new RegExp(keyword, "i");

    var _filter = {
      //多字段匹配
      $or: [
        {account: {$regex: reg}}
      ],
    };

    if(charOne){
      _filter['$or'].push({character: {$regex: charOne._id}})
    }

  }

  const list = await User.find(_filter)
    .sort({
      _id: -1,
    })
    .skip((page - 1) * size)
    .limit(size)
    .exec();

  if (keyword) {
    var total = await User.countDocuments(_filter).exec();
  } else {
    var total = await User.countDocuments().exec();
  }

  ctx.body = {
    data: {
      total,
      list,
      page,
      size,
    },
    msg: "获取列表成功",
    code: 1,
  };
  _filter = null;
  total = null;
});
router.delete("/:id", async (ctx) => {
  const {
    id
  } = ctx.params;

  const delMsg = await User.deleteOne({
    _id: id,
  });

  ctx.body = {
    data: delMsg,
    msg: "删除成功",
    code: 1,
  };
});
router.post("/add", async (ctx) => {
  const {
    account,
    password,
    character,
    classify = ""
  } = getBody(ctx);

  if (!account || !character || !password) {
    return;
  }

  const char = await Character.findOne({
    _id: character,
  });

  if (!char) {
    ctx.body = {
      code: 0,
      msg: "出错了",
      data: null,
    };
    return;
  }

  const findUser = await User.findOne({
    account,
  }).exec();

  if (findUser) {
    ctx.body = {
      code: 0,
      msg: "已存在该用户",
      data: null,
    };
    return;
  }

  const user = new User({
    account,
    password: password || config.DEFAULT_PASSWORD,
    character,
    classify,
  });

  const res = await user.save();

  changeCharacter(account, classify);

  ctx.body = {
    data: res,
    code: 1,
    msg: "添加成功",
  };
});
router.post("/reset/password", async (ctx) => {
  const {
    id
  } = getBody(ctx);

  const findUser = await User.findOne({
    _id: id,
  }).exec();

  if (!findUser) {
    ctx.body = {
      code: 0,
      msg: "找不到该用户",
    };
    return;
  }

  findUser.password = md5(config.DEFAULT_PASSWORD);

  const res = await findUser.save();
  ctx.body = {
    data: {
      account: res.account,
      _id: res._id,
    },
    code: 1,
    msg: "修改成功",
  };
});
router.post("/update/character", async (ctx) => {
  const {
    character,
    userId,
    classify = "",
    oldclassify = "",
    account,
  } = getBody(ctx);

  console.log({
    character,
    userId,
    classify,
    oldclassify,
    account,
  });

  const user = await User.findOne({
    _id: userId,
    account,
  });
  if (!user) {
    ctx.body = {
      code: 0,
      msg: "出错了",
      data: null,
    };
    return;
  }

  const char = await Character.findOne({
    _id: character,
  });
  if (!char) {
    ctx.body = {
      code: 0,
      msg: "出错了",
      data: null,
    };
    return;
  }

  changeCharacter(account, classify, oldclassify);

  user.character = character;
  user.classify = classify;

  const res = await user.save();

  ctx.body = {
    data: res,
    code: 1,
    msg: "修改成功",
  };
});
router.get("/info", async (ctx) => {
  //Authorization: Bearer $%^7865fdgshgfdh

  const tokenInfo = JSON.parse(JSON.stringify(await verify(getToken(ctx))));
  const userInfo = await User.findOne({
    account: tokenInfo.account,
  });

  Object.assign(tokenInfo, {
    classify: userInfo.classify,
  });
  ctx.body = {
    data: tokenInfo,
    code: 1,
    msg: "获取信息成功",
  };
});
router.post("/addMany", async (ctx) => {
  const {
    Key
  } = getBody(ctx);

  if (!Key) {
    return;
  }

  const path = `${config.UPLOAD_DIR}/${Key}`;
  const excel = loadExcel(path);
  const sheet = getFirstSheet(excel);

  const arr = [];
  // forEach 使用异步 会造成变量隔绝
  // 解决 for of
  for (item of sheet) {
    const [account, password = config.DEFAULT_PASSWORD, title = "普通用户"] = item;
    if (!account) {
      continue;
    }
    const character = await Character.findOne({
      title,
    }).exec();
    if (!character) {
      continue;
    }
    const findOne = await User.findOne({
      account,
    }).exec();
    if (findOne) {
      continue;
    }
    arr.push({
      account,
      password:md5(password),
      character: character._id,
      classify: "",
    });
  }
  await User.insertMany(arr);
  ctx.body = {
    data: null,
    code: 1,
    msg: "添加成功",
  };
});
module.exports = router;