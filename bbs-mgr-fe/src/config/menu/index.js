
export default [
  {
    title: '首页',
    url: '/dashboard',
    onlyAdmin: false
  },
  {
    title: '帖子管理',
    url: '/posts',
    onlyAdmin: true
  },
  {
    title: '评论管理',
    url: '/comment',
    onlyAdmin: true
  },
  {
    title: '公告管理',
    url: '/notices',
    onlyAdmin: true
  },
  {
    title: '板块管理',
    url: '/post-classify',
    onlyAdmin: true
  },
  {
    title: '用户管理',
    url: '/users',
    onlyAdmin: true
  },
  {
    title: '其他管理',
    url: '/applys',
    onlyAdmin: true,
    children: [
      {
        title: '重置密码管理',
        url: '/reset-password',
        onlyAdmin: true
      },
      {
        title: '邀请码管理',
        url: '/invite-code',
        onlyAdmin: true
      },
      {
        title: '版主申请管理',
        url: '/moder-apply',
        onlyAdmin: true
      }     
    ]
  },
  {
    title: '日志列表',
    url: '/records',
    onlyAdmin: true
  },
  {
    title: '个人设置',
    url: '/profile',
    onlyAdmin: false
  },
];
