import store from '@/store';

export const judge = () => {
    const uc = store.state.userCharacter;
    return uc.name
}

export const getCharacterInfoById = (id) => {
    const {characterInfo} = store.state;
    const one =  characterInfo.find((item) => {
        return item._id == id
    });
    
    return one || {title:"未知角色"}
}

export const isModerClassify = () => {
    const {userInfo, routeInfo} = store.state;
    return userInfo.classify == routeInfo
}

export const isAuthor = () => {
    const {userInfo, routeInfo} = store.state;
    return userInfo.account == routeInfo
}
