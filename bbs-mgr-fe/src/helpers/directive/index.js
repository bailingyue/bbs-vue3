import {judge, isModerClassify, isAuthor} from '@/helpers/character';
import store from '@/store';
// 自定义指令
export const regDirectives = (app) => {
    app.directive('only-admin', {
        mounted(el, { value = true}){
            const res = judge();
            if(res != 'admin' && value){
                el.style.display = 'none'
            }
        },
    });

    app.directive('only-power', {
        mounted(el, { value = true}){
            const res = judge();
            if(res == 'member' && value){
                el.style.display = 'none'
            }
        },
    });

    // 处理 版主-对应板块
    app.directive('only-classify', {
        mounted(el){
            const res = judge();
            const res2 = isModerClassify();
            if(res == 'moderator' && !res2){
                el.style.display = 'none'
            }
        },
    });

    // 处理普通用户-编辑的关系
    app.directive('only-self', {
        mounted(el){
            const res = judge();
            const res2 = isAuthor();
            if(res == 'member' && !res2){
                el.style.display = 'none'
            }
        },
    });
};
