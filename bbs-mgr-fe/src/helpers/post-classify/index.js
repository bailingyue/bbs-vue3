import store from '@/store';

export const getClassifyInfoById = (id) => {
    const {postClassifies} = store.state;
    const one =  postClassifies.find((item) => {
        return item._id == id
    });
    
    return one || {title:"无分类"}
}

export const getIdByClassifyInfo = (keyword) => {
    const {postClassifies} = store.state;
    const one =  postClassifies.find((item) => {
        return item.title == keyword
    });
    
    return one ? one._id : keyword
}