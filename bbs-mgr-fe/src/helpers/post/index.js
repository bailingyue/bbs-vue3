import store from '@/store';

export const getPostInfoById = (id, data) => {
    const one =  data.find((item) => {
        return item._id == id
    });
    
    return one || {title:"未知帖子"}
}

export const getIdByPostInfo = (keyword, data) => {
    const one =  data.find((item) => {
        return item.name == keyword
    });
    
    return one ? one._id : keyword
}