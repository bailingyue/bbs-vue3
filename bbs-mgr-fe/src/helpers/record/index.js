const LOG_MAP = [
    ['/character/list', '获取人物身份列表'],
    ['/post-classify/listAll', '进入板块管理'],
    ['/post/listAll', '进入帖子管理'],
    ['/comment/listAll', '进入评论管理'],
    ['/invite/list', '进入邀请码管理'],
    ['/logs/list', '进入帖子详情管理'],
    ['/moder-apply/list', '进入版主申请管理'],
    ['/notices/listAll', '进入公告管理'],
    ['/reset-password/list', '进入重置密码管理'],
    ['/user/list', '进入用户管理'],
    ['/user-score/score', '进入个人设置'],
    ['/dashboard/pics', '进入首页'],
    ['/records/list', '获取日志列表'],

];

export const getLogInfoByPath = (path) => {
    let title = '';
    
    LOG_MAP.forEach((item) => {
        if(path.includes(item[0])) {
            title = path.replace(item[0], item[1]);
        }
    });

    return title || path;
};