const TOKEN_STORAGE_KEY = '_tt';

export const getToken = () => {
    return localStorage.getItem(TOKEN_STORAGE_KEY) || '';
};

export const setToken = (token) => {
    localStorage.setItem(TOKEN_STORAGE_KEY, token);
};

export const clearToken = () => {
    localStorage.removeItem(TOKEN_STORAGE_KEY)
}