import { defineComponent, ref, reactive, onMounted} from "vue";
import { UserOutlined, } from "@ant-design/icons-vue";
import menu from '@/config/menu';
import { useRouter, useRoute, routerKey } from 'vue-router';

export default defineComponent({
  components: {
    UserOutlined
  },
  // 在created()生命周期之前执行
  // 定义methods、watch、computed、data数据都放在函数中
  setup() {
    const router = useRouter();
    const route = useRoute();
    
    const openKeys = ref([]);
    const selectedKeys = ref([]);

    const toTurn = (url) => {
        router.push(url)
    }

    // 保存路由状态
    onMounted(() => {
      console.log(route.path)
        selectedKeys.value = [route.path];

        menu.forEach((item) => {
          (item.children || []).forEach((child) => {
            if(child.url === route.path){
              openKeys.value.push(item.title)
            }
          })
        })
    })

    return {
        openKeys,
        selectedKeys,
        menu,
        toTurn
    }
  }
});
