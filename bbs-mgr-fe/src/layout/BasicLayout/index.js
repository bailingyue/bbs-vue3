import { defineComponent, ref, reactive, onMounted, onUpdated } from "vue";
import { useRouter } from "vue-router";
import leftNav from "./Nav/index.vue";
import store from "@/store";
import { setToken, getToken } from "../../helpers/token";
import { message, Modal } from "ant-design-vue";
import { messages } from "@/service";
import { result, formatTimestamp } from "@/helpers/utils";

export default defineComponent({
  components: {
    leftNav
  },
  setup() {
    const router = useRouter();
    const { postClassifies, userInfo, userCharacter } = store.state;
    const ruleText = ref("");
    const toal = ref(0);

    const logout = () => {
      setToken("");
      router.replace("/auth");
    };

    const loadingTxt = () => {
      const Name = userCharacter.name;
      if (Name != "moderator") {
        if (Name == "admin") {
          ruleText.value = "拥有全部权限。";
          return;
        }
        ruleText.value = "拥有基本权限，无法进行管理操作。";
        return;
      }
      const one = postClassifies.find(item => item._id == userInfo.classify);

      if (one) {
        ruleText.value = `拥有基本权能和部分管理权限，无法进行危险性操作。您管理的板块：${one.title}。`;
      } else {
        ruleText.value = `拥有基本权能和部分管理权限，无法进行危险性操作。您尚未被分配。`;
      }
    };

    const getToal = async () => {
      const res = await messages.count();
      result(res).success(({ data:{
        like_total,
        comment_total
      } }) => {
        toal.value = like_total + comment_total;
      });
    };

    const showRule = () => {
      Modal.confirm({
        title: `你好，${userCharacter.title}`,
        content: (
          <div>
            <p>权限：{ruleText.value}</p>
          </div>
        )
      });
    };

    onMounted(() => {
      loadingTxt();
      getToal();
    });

    onUpdated(() => {
      getToal();
    });

    return {
      logout,
      store: store.state,
      showRule,
      toal
    };
  }
});
