import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import spaceBetween from './components/spaceBetween/index.vue';
import FlexEnd from './components/FlexEnd/index.vue';
import FlexCenter from './components/FlexCenter/index.vue';
import { regDirectives } from '@/helpers/directive';

const app = createApp(App);
// 自定义指令
regDirectives(app);

app
  .use(store)
  .use(router)
  .use(Antd)
  .component('space-between', spaceBetween)
  .component('flex-end', FlexEnd)
  .component('flex-center', FlexCenter)
  .mount('#app');
