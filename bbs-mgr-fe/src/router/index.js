import {
  createRouter,
  createWebHashHistory
} from 'vue-router';

import store from '@/store';
import { message } from "ant-design-vue";
import { user } from '@/service';

const routes = [{
    path: '/auth',
    name: 'Auth',
    component: () => import('../views/Auth/index.vue'),
  },

  {
    path: '/',
    name: 'BasicLayout',
    redirect: '/auth',
    component: () => import('../layout/BasicLayout/index.vue'),
    children: [
      {
        path: '/dashboard',
        name: 'DashBoard',
        component: () => import('../views/DashBoard/index.vue'),
      },
      {
        path: '/datashow/post/:id',
        name: 'DataShow',
        component: () => import('../views/DataShow/index.vue'),
      },
      {
        path: '/posts',
        name: 'Posts',
        component: () => import('../views/Posts/index.vue'),
      },
      {
        path: '/posts/:id',
        name: 'PostDetail',
        component: () => import('../views/PostDetail/index.vue'),
      },
      {
        path: '/users',
        name: 'Users',
        component: () => import('../views/Users/index.vue'),
      },
      {
        path: '/records',
        name: 'Records',
        component: () => import('../views/Records/index.vue'),
      },
      {
        path: '/invite-code',
        name: 'InviteCode',
        component: () => import('../views/InviteCode/index.vue'),
      },
      {
        path: '/reset-password',
        name: 'ResetPassword',
        component: () => import('../views/ResetPassword/index.vue'),
      },
      {
        path: '/moder-apply',
        name: 'ModerApply',
        component: () => import('../views/ModerApply/index.vue'),
      },
      {
        path: '/post-classify',
        name: 'PostClassify',
        component: () => import('../views/PostClassify/index.vue'),
      },
      {
        path: '/profile',
        name: 'Profile',
        component: () => import('../views/Profile/index.vue'),
      }, 
      {
        path: '/notices',
        name: 'Notices',
        component: () => import('../views/Notices/index.vue'),
      }, 
      {
        path: '/dataShowDetail/:id/:classify',
        name: 'DataShowDetail',
        component: () => import('../views/DataShowDetail/index.vue'),
      }, 
      {
        path: '/Comment',
        name: 'Comment',
        component: () => import('../views/Comment/index.vue'),
      }, 
      {
        path: '/Message',
        name: 'Message',
        component: () => import('../views/Message/index.vue'),
      }, 
      // {
      //   path: '/SendShow',
      //   name: 'SendShow',
      //   component: () => import('../views/SendShow/index.vue'),
      // }, 
    ],

  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});




router.beforeEach(async (to, from, next) => {
  // 拿到状态信息 
  let res = {};

  try{
    // 此操作仍然会携带token去访问
    res = await user.info();
  } catch (e) {
    console.log(e.message)
    if (e.message.includes('code 401')){
      res.code = 401;
    }
  }

  const {code} = res;
  
  // 进入页面后 删掉token 防白屏措施
  if(code == 401){
    if(to.path === '/auth') {
      next();
      return;
    }

    message.error("认证失败, 请重新登录")
    next('/auth');
  }

  // 一些全局信息应该在进入页面后加载 有先后顺序
  if(!store.state.characterInfo.length){
    await store.dispatch('getCharacterInfo');
  }
  if(!store.state.userInfo.account){
    await store.dispatch('getUserInfo');
  }

  if(!store.state.postClassifies.length){
    await store.dispatch('getPostClassifies');
  }

  // 当成功登录后 再去访问auth 自动跳转到主页
  if (to.path === '/auth') {
    next('/dashboard')
    return;
  }
  next();
})

export default router;


