import {
    del, post, get
  } from '@/helpers/request';
  
export const list = (form) => {
  return get('/comment/list', form); 
};

export const listAll = (form) => {
  return get('/comment/listAll', form); 
};

export const add = (form) => {
  return post('/comment/add', form); 
};

export const update = (form) => {
  return post('/comment/update', form); 
};

export const remove = (id) => {
  return del(`/comment/${id}`,); 
};

export const updateStatus = (form) => {
  return post('/comment/update/status', form); 
};

export const addMany = (form) => {
  return post(`/comment/addMany`, form); 
};


