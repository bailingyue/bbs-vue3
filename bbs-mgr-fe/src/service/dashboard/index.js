import {
  del, post, get
} from '@/helpers/request';


export const list = () => {
  return get('/dashboard/base-info'); 
};

export const pics = () => {
  return get('/dashboard/pics'); 
};

