// 把auth下的所有导出内容聚合到一个对象中
export * as auth from './auth';
export * as post from './post';
export * as logs from './logs';
export * as user from './user';
export * as character from './character';
export * as records from './records';
export * as resetPassword from './reset-password';
export * as inviteCode from './invite-code';
export * as postClassify from './post-classify';
export * as profile from './profile';
export * as dashboard from './dashboard';
export * as datashow from './datashow';
export * as notices from './notices';
export * as moderApply from './moder-apply';
export * as comment from './comment';
export * as userScore from './user-score';
export * as messages from './messages';
