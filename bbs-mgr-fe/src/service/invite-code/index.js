import {
  del, post, get
} from '@/helpers/request';


export const list = (form) => {
  return get('/invite/list', form); 
};

export const add = (form) => {
  return post('/invite/add', form); 
};

export const remove = (id) => {
  return del(`/invite/${id}`); 
};