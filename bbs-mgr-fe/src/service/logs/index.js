import {
  del, post, get
} from '@/helpers/request';

export const list = (form) => {
  return get('/logs/list', form); 
};

