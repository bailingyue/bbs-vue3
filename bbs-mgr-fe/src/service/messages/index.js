import {
    del, post, get
  } from '@/helpers/request';
  
  export const like = () => {
    return get('/messages/like'); 
  };

  export const comment = () => {
    return get('/messages/comment'); 
  };

  export const count = () => {
    return get('/messages/count'); 
  };
  
  export const updateStatus = (form) => {
    return post('/messages/update/status', form); 
  };
  