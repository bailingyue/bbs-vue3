import {
    del, post, get
  } from '@/helpers/request';
  
  export const list = (form) => {
    return get('/moder-apply/list', form); 
  };
  
  export const update = (form) => {
    return post('/moder-apply/update/status', form); 
  };
  
  