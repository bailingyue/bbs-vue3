import {
    del, post, get
  } from '@/helpers/request';
  
export const list = (form) => {
  return get('/notices/list', form); 
};

export const listAll = (form) => {
  return get('/notices/listAll', form); 
};

export const add = (form) => {
  return post('/notices/add', form); 
};

export const update = (form) => {
  return post('/notices/update', form); 
};

export const remove = (id) => {
  return del(`/notices/${id}`,); 
};

export const updateStatus = (form) => {
  return post('/notices/update/status', form); 
};

export const addMany = (form) => {
  return post(`/notices/addMany`, form); 
};


