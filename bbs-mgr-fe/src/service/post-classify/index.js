import {
  del, post, get
} from '@/helpers/request';

export const list = (form) => {
  return get('/post-classify/list', form); 
};

export const add = (form) => {
  return post('/post-classify/add', form); 
};

export const remove = (form) => {
  return post('/post-classify/delete', form); 
};

export const update = (form) => {
  return post('/post-classify/update', form); 
};

export const listAll = () => {
  return get('/post-classify/listAll'); 
};

export const listModerator = () => {
  return get('/post-classify/moderator-list'); 
};