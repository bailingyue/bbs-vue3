import {
  del, post, get
} from '@/helpers/request';

export const add = (form) => {
  return post('/post/add', form); 
};

export const list = (form) => {
  return get('/post/list',form); 
};

export const listAll = (form) => {
  return get('/post/listAll',form); 
};


export const remove = (id) => {
  return del(`/post/${id}`,); 
};

export const update = (form) => {
  return post('/post/update', form); 
};

export const detail = (id) => {
  return get(`/post/detail/${id}`,
  ); 
}

export const addMany = (form) => {
  return post(`/post/addMany`, form); 
};


export const updateStatus = (form) => {
  return post('/post/update/status', form); 
};


