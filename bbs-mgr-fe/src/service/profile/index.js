import {
  del, post, get
} from '@/helpers/request';

export const update = (form) => {
  return post(`/profile/update/password`,form); 
};

export const apply = (form) => {
  return post(`/profile/update/apply`,form); 
};
