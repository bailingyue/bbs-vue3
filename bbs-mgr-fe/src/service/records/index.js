import {
  del, post, get
} from '@/helpers/request';

export const list = (form) => {
  return get('/records/list', form); 
};


export const remove = (id) => {
  return post(`/records/delete`, {
    id
  }); 
};

