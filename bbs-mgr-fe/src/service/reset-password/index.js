import {
  del, post, get
} from '@/helpers/request';

export const list = (form) => {
  return get('/reset-password/list', form); 
};

export const add = (form) => {
  return post('/reset-password/add', form); 
};

export const update = (form) => {
  return post('/reset-password/update/status', form); 
};

