import {
    del, post, get
  } from '@/helpers/request';
  
  export const score = () => {
    return get('/user-score/score'); 
  };

  