import {
  del, post, get
} from '@/helpers/request';

export const list = (data) => {
  return get('/user/list', data); 
};

export const remove = (id) => {
  return del(`/user/${id}`,
  ); 
};

export const add = (form) => {
  return post(`/user/add`,form); 
};

export const resetPassword = (id) => {
  return post(`/user/reset/password`,{
    id,
  }); 
};

export const info = () => {
  return get('/user/info'); 
};


export const editCharacter = (form) => {
  return post(`/user/update/character`, form); 
};

export const addMany = (form) => {
  return post(`/user/addMany`, form); 
};

