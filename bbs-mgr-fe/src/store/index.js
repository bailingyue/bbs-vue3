import { createStore } from 'vuex';
import { character, user, postClassify } from '@/service';
import { result } from '@/helpers/utils';
import { getCharacterInfoById } from '@/helpers/character';
import {useRoute} from 'vue-router';


export default createStore({
  state: {
    characterInfo: [],
    userInfo:{},
    userCharacter:{},
    postClassifies:[],
    routeInfo:""
  }, 
  mutations: {
    setCharacterInfo(state, characterInfo){
      state.characterInfo = characterInfo;
    },
    setUserInfo(state, userInfo){
      state.userInfo = userInfo;
    },
    setUserCharacter(state, userCharacter){
      state.userCharacter = userCharacter;
    },
    setPostClassifies(state, postClassifies){
      state.postClassifies = postClassifies;
    },
    setRouteInfo(state, routeInfo){
      state.routeInfo = routeInfo;
    },
  },
  actions: {
    async getPostClassifies(store){
      const res = await postClassify.listAll();
      result(res)
        .success(({ data }) => {
          console.log(data)
          store.commit('setPostClassifies', data)
        })
    },

    async getCharacterInfo(store){
      const res = await character.list();
      result(res)
        .success(({ data }) => {
          store.commit('setCharacterInfo', data)
        })
    },
    async getUserInfo(store){
      const res = await user.info();
      result(res)
        .success(async ({ data }) => {
          store.commit('setUserInfo', data)
          store.commit('setUserCharacter', getCharacterInfoById(data.character))
        })
    }
  },
});
