import {defineComponent,reactive,onMounted, ref } from 'vue';
import { UserOutlined ,KeyOutlined , EditOutlined } from '@ant-design/icons-vue';
import {auth} from '@/service';
import {result} from '@/helpers/utils';
import {message} from 'ant-design-vue';
import store from '@/store';
import { getCharacterInfoById } from '@/helpers/character';
import {stringifyQuery, useRouter} from 'vue-router';
import { setToken, clearToken} from '@/helpers/token';
import { resetPassword } from "@/service";
import md5 from 'md5';

export default defineComponent({

  components:{
    UserOutlined,
    KeyOutlined,
    EditOutlined
  },
  // 项目初始化只会执行一次
  setup(){
    const router = useRouter();
    const showResetModal = ref(false);
    const resetValue = ref('');

    const regForm = reactive({
      account:'',
      password:'',
      inviteCode: ''
    });

    const logForm = reactive({
      account:'',
      password:''
    });


    const register = async () =>{

      // 客户端提前判断
      if (regForm.account === ''){
        message.info('请输入账户');
        return;
      }
      if (regForm.password === ''){
        message.info('请输入密码');
        return;
      }
      if (regForm.inviteCode === ''){
        message.info('请输入邀请码');
        return;
      }

      // 字符不能为非法字符
      let acc_re = "^[A-Za-z0-9]+$";
      if(!regForm.account.match(acc_re)){
      　//由数字和26个英文字母组成的字符串
          message.info('账户名必须由数字或英文字母组成');
        return;  
      }

      let pas_re = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$";
      if(!regForm.password.match(pas_re) || regForm.password.length < 8){
        　message.info('密码必须由数字和英文字母组成，长度为8-16');
        return;  
      }

      const res = await auth.register(
        regForm.account, 
        md5(regForm.password),
        regForm.inviteCode
      );
      result(res)
        .success((data) => {
          message.success(data.msg);
        })
    };

    const login = async () =>{
      
      // 客户端提前判断
      if (logForm.account === ''){
        message.info('请输入账户');
        return;
      }
      if (logForm.passward === ''){
        message.info('请输入密码');
        return;
      }

      const res = await auth.login(logForm.account, md5(logForm.password))
      result(res)
        .success(async ({msg, data:{user, token}}) => {
          message.success(msg);
          setToken(token);
          await store.dispatch('getCharacterInfo');
          store.commit('setUserInfo', user);
          console.log("auth", user.character)
          store.commit('setUserCharacter', getCharacterInfoById(user.character));
          console.log(store.state);
          router.replace('/dashboard');
        })
    }

    // onMounted(() => {
    //   clearToken();
    // })

    const submit = async () => {
      if(!resetValue.value){
        message.error("申请的账号不能为空")
        return;
      }
      const res = await resetPassword.add({
        account: resetValue.value
      })

      result(res)
      .success(({msg}) => {
        message.success(msg)
      })

      showResetModal.value = false;
    }

    return{
      regForm,
      register,
      logForm,
      login,
      showResetModal,
      submit,
      resetValue
    }
  }
});
