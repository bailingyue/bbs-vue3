import { defineComponent, reactive, ref, onMounted } from "vue";
import { comment } from "@/service";
import { result, clone } from "@/helpers/utils";
import { message } from "ant-design-vue";
import store from "@/store";

const defaultFormData = reactive({
  postId: "",
  author:"",
  content: "",
  status: ""
});

export default defineComponent({
  props: {
    show: Boolean,
    tempPost: Array
  },
  setup(props, context) {
    // 生拷贝防止reactive对其造成影响
    const addForm = reactive(clone(defaultFormData));

    const submit = async () => {
      let form = clone(addForm);

      if(!form.postId  || !form.content || !form.author){
        message.error('值不能为空')
        return;
      }
      Object.assign(form, {
        isManage:true
      })
      const res = await comment.add(form);
      result(res).success(data => {
        // 清空表单
        Object.assign(addForm, defaultFormData);
        message.success(data.msg);
        close();
        context.emit("getList");
      });
    };

    const close = () => {
      context.emit("update:show", false);
    };

    return {
      addForm,
      submit,
      props,
      close,
      store: store.state
    };
  }
});
