import { defineComponent, ref, onMounted } from "vue";
import { message, Modal } from "ant-design-vue";
import { comment, post } from "@/service";
import { result, formatTimestamp } from "@/helpers/utils";
import { useRouter } from "vue-router";
import addOne from "./AddOne/index.vue";
import update from "./Update/index.vue";
import { getClassifyInfoById, getIdByClassifyInfo } from "@/helpers/post-classify";
import { getCharacterInfoById } from "@/helpers/character";
import { getPostInfoById, getIdByPostInfo } from "@/helpers/post";
import store from '@/store';

/*
    useRoute
      当前页面的路由
    useRouter
      当前页面的方法 如前进后退
  */
const columns = [
  {
    title: "帖子",
    slots: {
      customRender: "postId"
    }
  },
  {
    title: "评论人",
    dataIndex: "author"
  },
  {
    title: "发送时间",
    slots: {
      customRender: "createdAt"
    }
  },
  {
    title: "分类",
    slots: {
      customRender: "classify"
    }
  },
  {
    title: "内容",
    dataIndex: "content",
    ellipsis: true
  },
  {
    title: "点赞数",
    dataIndex: "likes"
  },
  {
    title: "操作",
    slots: {
      customRender: "actions"
    }
  }
];
export default defineComponent({
  components: {
    addOne,
    update
  },

  setup() {
    const show = ref(false);
    const isSearch = ref(false);
    const list = ref([]);
    const {postClassifies} = store.state;

    const keyword = ref("");
    const showUpdateModel = ref(false);
    const curEditList = ref({});
    const tempPost = ref([]);

    const getList = async keyword => {
      const res = await comment.listAll({
        keyword
      });
      result(res).success(({ data }) => {
        list.value = data;
                console.log(data)
      });
    };

    const getPost = async keyword => {
      const res = await post.listAll({
        keyword
      });
      result(res).success(({ data }) => {
        tempPost.value = data;
      });
    };

    onMounted(() => {
      getList();
      getPost();
    });

    const onSearch = async () => {
      // !! 转化成bool类型 隐式转化
      isSearch.value = !!keyword.value;

      const one  = postClassifies.find(item => item.title == keyword.value);
      if(one){
        var key = getIdByClassifyInfo(keyword.value);
      }else{
        var key = getIdByPostInfo(keyword.value, tempPost.value);
      }
     
      getList(key);
    };

    const backAll = () => {
      isSearch.value = false;
      keyword.value = "";
      getList();
    };

    const remove = async ({ text: record }) => {
      const { _id } = record;
      const res = await comment.remove(_id);
      result(res).success(({ msg }) => {
        message.success(msg);
        getList();
      });
    };

    const update = ({ record }) => {
      showUpdateModel.value = true;
      curEditList.value = record;
    };

    const updateCurList = newData => {
      Object.assign(curEditList.value, newData);
    };


    const changeStatus = async ({ record }) => {
      const res = await comment.updateStatus({
        id: record._id,
        status: !record.status
      });
      result(res).success(({ msg }) => {
        message.success(msg);
        getList();
      });
    };

    return {
      columns,
      show,
      list,
      formatTimestamp,
      getList,
      keyword,
      isSearch,
      onSearch,
      backAll,
      remove,
      showUpdateModel,
      update,
      curEditList,
      updateCurList,
      getClassifyInfoById,
      getCharacterInfoById,
      changeStatus,
      tempPost,
      getPostInfoById
    };
  }
});
