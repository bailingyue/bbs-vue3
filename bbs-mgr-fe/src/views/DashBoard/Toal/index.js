import { defineComponent, ref, onMounted} from "vue";
import { dashboard } from "@/service";
import { result } from "@/helpers/utils";

export default defineComponent({
  setup() {

    const list = ref([]);
    const getList = async () => {
        const res = await dashboard.list();
        result(res).success(({ data }) => {
          list.value = data;
        });
      };
      
      onMounted(() => {
        getList();
      });

    return {
        list,
        getList
    };
  }
});
