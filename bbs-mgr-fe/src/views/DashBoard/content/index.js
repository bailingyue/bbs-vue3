import { defineComponent, ref, onMounted } from "vue";
import { LeftCircleOutlined, RightCircleOutlined } from '@ant-design/icons-vue';
import { dashboard } from "@/service";
import { result, formatTimestamp} from "@/helpers/utils";
import { message } from "ant-design-vue";
import Posts from './posts/index.vue';
import AddOne from './posts/AddOne/index.vue';
import { getHeaders } from "@/helpers/request";

// import {} from '@ant-design/icons-vue';

export default defineComponent({
  components: {
    LeftCircleOutlined, 
    RightCircleOutlined,
    Posts,
    AddOne
  },

  setup() {
    const addShow = ref(false);
    const list = ref([]);
    
    const getPics = async () => {
      const res = await dashboard.pics();
      result(res).success( async ({data}) => {
        list.value = data
      })
    }

    const onUploadChange = ({ file }) => {
      if (file.status == "removed") {
        message.success("删除成功");
        return;
      }
      if (file.response) {
        getPics();
      }
    };

    onMounted( () => {
      getPics();
    })

    return {
      addShow,
      list,
      headers: getHeaders(),
      onUploadChange
    };
  }
});
