import {LikeOutlined, MessageOutlined, LikeFilled } from "@ant-design/icons-vue";
import { defineComponent, ref, onMounted, watch } from "vue";
import { message } from "ant-design-vue";
import { post } from "@/service";
import { result, formatTimestamp } from "@/helpers/utils";
import { useRouter, useRoute } from "vue-router";
import AddOne from './AddOne/index.vue';
import { getClassifyInfoById, getIdByClassifyInfo } from "@/helpers/post-classify";

// import router from '../../router';


export default defineComponent({
  components: {
    LikeFilled,
    LikeOutlined,
    MessageOutlined,
    AddOne
  },

  setup(props, ctx) {
    const route = useRoute();
    const router = useRouter();
    const list = ref([]);
    const addShow = ref(false);
    const keyword = ref("");
    const isSearch = ref(false);
    
    const getList = async (keyword, bySort) => {

      const res = await post.list({
        keyword,
        bySort,
      });
      result(res).success(({ data }) => {
        list.value = data
        console.log(data)
      });
    };

    onMounted(() => {
      getList();
    });

    const toDetail = (item) => {
      router.push(`/dataShowDetail/${item._id}/${item.classify}`);
    };

    const changeLikes = async (item) => {
      const res = await post.update({
        id: item._id,
        // 将要置为的状态
        furStatus: !item.isLike,
        isForward: true
      });
      result(res).success(async ({ data }) => {
        const idx = list.value.findIndex( i => {
          return i._id === item._id;
        })
        list.value[idx].likes = data.likes;
        list.value[idx].isLike = !item.isLike;
      });
    }

    const changeStatus = async (item, status) => {
      
      const res = await post.updateStatus({
        id: item._id,
        status
      });
      result(res).success(({ msg }) => {
        message.success(msg);
        getList();
      });
    }

    const onSearch = () => {
      // !! 转化成bool类型 隐式转化
      isSearch.value = !!keyword.value;

      const key = getIdByClassifyInfo(keyword.value);
      console.log(key);

      getList(key);
    };

    
    const backAll = () => {
      isSearch.value = false;
      keyword.value = "";
      getList();
    };

    
    const bySort = (status) => {
      if(status){
        getList('', 1);
      }else{
        getList();
      }
    }
    

    return {
      list,
      toDetail,
      formatTimestamp,
      changeLikes,
      changeStatus,
      addShow,
      getList,
      keyword,
      isSearch,
      onSearch,
      backAll,
      bySort,
      getClassifyInfoById
    };
  }
});
