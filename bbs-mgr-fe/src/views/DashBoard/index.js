import { defineComponent} from "vue";
import Toal from './Toal/index.vue';
import postClassify from './postClassify/index.vue';
import notices from './notices/index.vue';
import content from './content/index.vue';

export default defineComponent({
  components: {
    Toal,
    postClassify,
    notices,
    content
  },

  setup() {
  }
});
