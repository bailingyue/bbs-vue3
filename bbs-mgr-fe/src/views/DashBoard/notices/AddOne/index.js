import { defineComponent, reactive, ref, onMounted } from "vue";
import { notices } from "@/service";
import { result, clone } from "@/helpers/utils";
import { message } from "ant-design-vue";
import store from "@/store";

const defaultFormData = {
  title: "",
  content: "",
  // date: "",
  classify: ""
};

export default defineComponent({
  props: {
    show: Boolean,
    classify: String
  },
  // 在created()生命周期之前执行
  // 定义methods、watch、computed、data数据都放在函数中
  setup(props, context) {
    const addForm = reactive(clone(defaultFormData));

    const close = () => {
      context.emit("update:show", false);
    };

    const submit = async () => {
      // console.log(addForm)
      const form = clone(addForm);
      Object.assign(form, {
        classify: props.classify
      })
      if(!form.title || !form.content){
        message.error("值不能为空")
        return;
      }
      // 时间戳 1970年开始的秒数 
      // if(!addForm.date){
      //   form.date = (new Date()).valueOf();
      // }else{
      //   form.date = addForm.date.valueOf();
      // }
      
      const res = await notices.add(form);
      result(res).success(({msg}) => {
        // 清空表单
        message.success(msg);
        context.emit("getNotice");
        Object.assign(addForm, defaultFormData);
        close();
      });
    };

    return {
      submit,
      close,
      addForm,
      props
    };
  }
});
