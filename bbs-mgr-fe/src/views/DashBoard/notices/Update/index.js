import { defineComponent, reactive, ref, onMounted, watch } from "vue";
import { notices } from "@/service";
import { result, clone } from "@/helpers/utils";
import { message } from "ant-design-vue";
import store from "@/store";
import moment from "moment";

const defaultFormData = {
  title: "",
  content: "",
  date: "",
  classify: ""
};

export default defineComponent({
  props: {
    show: Boolean,
    notice: Object
  },
  // 在created()生命周期之前执行
  // 定义methods、watch、computed、data数据都放在函数中
  setup(props, context) {
    const editForm = reactive(clone(defaultFormData));

    const close = () => {
      context.emit("update:show", false);
    };

    watch(
      () => props.notice,
      current => {
        // console.log("---------", current);
        Object.assign(editForm, current);
        // 把时间戳转化为正常时间
        // editForm.date = moment(Number(editForm.date));
        // editForm.date = moment((new Date()).valueOf());
      }
    );

    const submit = async () => {
      if (
        !editForm.title ||
        !editForm.content
      ) {
        message.error("值不能为空");
        return;
      }

      const res = await notices.update({
        id: props.notice._id,
        title: editForm.title,
        content: editForm.content,
        classify: editForm.classify
      });
      result(res).success(({ data, msg }) => {
        // 清空表单
        message.success(msg);
        context.emit("update", data);
        close();
      });
    };

    return {
      submit,
      close,
      editForm,
      props
    };
  }
});
