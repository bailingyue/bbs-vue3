import { defineComponent} from "vue";
import { datashow } from "@/service";
import { result} from "@/helpers/utils";
import { message } from "ant-design-vue";
import store from "@/store";
import { useRouter } from "vue-router";

export default defineComponent({
  setup() {
    const router = useRouter();

    const showPosts = async ({ title, _id }) => {
      const res = await datashow.list({
        title,
        id: _id
      });
      result(res).success(({ msg }) => {
        message.success(msg);
        router.push(`/datashow/post/${_id}`);
      });
    };

    return {
      store: store.state,
      showPosts
    };
  }
});
