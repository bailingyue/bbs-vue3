import { defineComponent, ref, onMounted } from "vue";
import { result, formatTimestamp } from "@/helpers/utils";
import { message } from "ant-design-vue";
import { useRoute } from "vue-router";
import {getClassifyInfoById} from "@/helpers/post-classify";
import Notices from './notices/index.vue';
import Posts from './posts/index.vue';
import store from '@/store';

// import {} from '@ant-design/icons-vue';

export default defineComponent({
  components: {
    Posts,
    Notices
  },
  setup() {
    const route = useRoute();
    const classify = route.params.id; 
    onMounted(() => {
      store.commit('setRouteInfo', classify);
    })

    return {
      classify,
      getClassifyInfoById
    };
  }
});


