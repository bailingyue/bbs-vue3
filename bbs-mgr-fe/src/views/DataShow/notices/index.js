import { defineComponent, ref, onMounted } from "vue";
import { notices } from "@/service";
import { result, formatTimestamp } from "@/helpers/utils";
import { message, Modal } from "ant-design-vue";
// import {} from '@ant-design/icons-vue';
import AddOne from "./AddOne/index.vue";
import Update from "./Update/index.vue";
import { useRoute } from "vue-router";

export default defineComponent({
  components: {
    AddOne,
    Update
  },
  setup() {
    const list = ref([]);
    const updateShow = ref(false);
    const editList = ref({});
    const addShow = ref(false);
    const route = useRoute();
    const classifyId = route.params.id;

    const getList = async () => {
      // console.log(props.classify)
      const res = await notices.list({
        classify: classifyId
      });
      result(res).success(({ data }) => {
        list.value = data.slice(0, 6);
        console.log(list.value)
      });
    };

    onMounted(() => {
      getList();
    });

    const update = (item) => {
      updateShow.value = true;
      editList.value = item;
    };

    const updateList = newData => {
      Object.assign(editList.value, newData);
    };

    // 进入详情页
    const toDetail = item => {
      Modal.confirm({
        title: `标题：${item.title}`,
        content: (
          <div>
            <p>发布者：{item.author || "暂无"}</p>
            <p>公告内容：{item.content || "暂无"}</p>
            <p>发布时间：{formatTimestamp(item.meta.updatedAt)|| "暂无"}</p>
          </div>
        )
      });
    };
    const changeStatus = async (item, status) => {
      
      const res = await notices.updateStatus({
        id: item._id,
        status
      });
      result(res).success(({ msg }) => {
        message.success(msg);
        getList();
      });

    }

    const remove = async ({ _id }) => {
      const res = await notices.remove(_id);
      result(res).success(({ msg }) => {
        message.success(msg);
        getList();
      });
    };
    
    return {
      list,
      toDetail,
      updateShow,
      updateList,
      editList,
      addShow,
      getList,
      update,
      remove,
      classify:classifyId,
      changeStatus
    };
  }
});
