import { defineComponent, reactive, ref, onMounted } from "vue";
import { post } from "@/service";
import { result, clone } from "@/helpers/utils";
import { message } from "ant-design-vue";
import store from "@/store";
import { useRoute } from "vue-router";

const defaultFormData = {
  name: "",
  // 发帖人
  author: "",
  // 发帖内容
  content: "",
  // 发帖时间
  date: "",
  // 分类
  classify: "",
  // // 附件
  // attachment: String,
  // 浏览数
  browses: 0,
  // 评论数
  comments: 0,
  // 点赞数
  likes: 0
};

export default defineComponent({
  props: {
    show: Boolean
  },
  setup(props, context) {
    // 生拷贝防止reactive对其造成影响
    const addForm = reactive(clone(defaultFormData));
    const {userCharacter} = store.state;
    const route = useRoute();

    try{
      addForm.classify = store.state.postClassifies[0]._id;
    }catch{
      addForm.classify = '';
    }

    const submit = async () => {
      const form = clone(addForm);
      if (!form.classify || !form.content || !form.name) {
        message.error("不能为空");
        return;
      }

      if(!form.date){
        form.date = (new Date()).valueOf();
      }else{
        // 时间戳 1970年开始的秒数
        form.date = addForm.date.valueOf();
      }
      if(!form.author){
        form.author = store.state.userInfo.account;
      }

      if (userCharacter.name == "member") {
        form.classify = route.params.id
      }

      const res = await post.add(form);
      result(res).success(data => {
        // 清空表单
        Object.assign(addForm, defaultFormData);
        try{
          addForm.classify = store.state.postClassifies[0]._id;
        }catch{
          addForm.classify = '';
        }
        message.success(data.msg);
        close();
        context.emit("getPost");
      });
    };

    const close = () => {
      context.emit("update:show", false);
    };

    return {
      addForm,
      submit,
      props,
      close,
      store: store.state
    };
  }
});
