import { defineComponent, reactive, watch } from "vue";
import { post } from "@/service";
import { result, clone } from "@/helpers/utils";
import { message } from "ant-design-vue";
import moment from "moment";
import store from "@/store";

const defaultFormData = reactive({
  name: "",
  author: "",
  content: "",
  date: "",
  classify: "",
  browses: 0,
  likes: 0
});
export default defineComponent({
  components: {},
  props: {
    show: Boolean,
    post: Object
  },
  setup(props, context) {
    const editForm = reactive(clone(defaultFormData));
    try{
      editForm.classify = store.state.postClassifies[0]._id;
    }catch{
      editForm.classify = '';
    }

    const close = () => {
      context.emit("update:show", false);
    };

    watch(
      () => props.post,
      current => {
        // console.log("---------", current);
        Object.assign(editForm, current);
        // 把时间戳转化为正常时间
        editForm.date = moment(Number(editForm.date));
      }
    );

    const submit = async () => {
      if (
        !editForm.name ||
        !editForm.author ||
        !editForm.content ||
        !editForm.date ||
        !editForm.classify
      ) {
        message.error("值不能为空");
        return;
      }
      // ...  合并对象
      const res = await post.update({
        id: props.post._id,
        name: editForm.name,
        author: editForm.author,
        content: editForm.content,
        date: editForm.date.valueOf(),
        classify: editForm.classify,
        browses: editForm.browses,
        likes: editForm.likes,
        isForward: true
      });
      result(res).success(({ data, msg }) => {
        // console.log(data)
        message.success(msg);
        context.emit("update", data);
        // Object.assign(editForm, defaultFormData);
        close();
      });
    };

    return {
      editForm,
      submit,
      props,
      close,
      store: store.state
    };
  }
});
