import { defineComponent, ref, onMounted } from "vue";
import { comment } from "@/service";
import { result, formatTimestamp, clone } from "@/helpers/utils";
import { message,Modal } from "ant-design-vue";
// import {} from '@ant-design/icons-vue';
import { LikeFilled, LikeOutlined } from "@ant-design/icons-vue";
import { useRouter, useRoute } from "vue-router";
import { getClassifyInfoById, getIdByClassifyInfo } from "@/helpers/post-classify";

export default defineComponent({
  components: {
    LikeFilled,
    LikeOutlined
  },
  // 在created()生命周期之前执行
  // 定义methods、watch、computed、data数据都放在函数中
  setup() {
    const list = ref([]);
    const total = ref(0);
    const inputText = ref("");
    const route = useRoute();
    const postId = route.params.id;
    const tempText = ref('');
    const keyword = ref("");
    const isSearch = ref(false);

    const getList = async (keyword, bySort) => {
      const res = await comment.list({
        keyword,
        postId,
        bySort
      });
      result(res).success(({data:{list:L,total:T}}) => {
        list.value = L;
        total.value = T;
        
      });
    };

    const remove = async ({_id}) => {
      const res = await comment.remove(_id)
      result(res)
       .success(({data , msg}) => {
         message.success(msg);
         getList();
       })
    }

    onMounted(() => {
      getList();
    });

    const changeLikes = async item => {
      console.log(item)
      const res = await comment.update({
        id: item._id,
        postId,
        // 将要置为的状态
        furStatus: !item.isLike,
      });
      result(res).success(async ({ data }) => {
        const idx = list.value.findIndex( i => {
          return i._id === item._id;
        })
        list.value[idx].likes = data.likes;
        list.value[idx].isLike = !item.isLike;
      });
    };
    const submit = async () => {
      // 简单判断输入的内容是否无意义
      const re = /(.)\1{3,}/u;
      if(inputText.value.length <= 5){
        message.error("输入的内容长度过短")
      }
      if(re.test(inputText.value) || tempText.value == inputText.value){
        message.error("不要连续输入相同的内容")
        // 正则后面的u修饰符是用来支持Unicode宽字符的{3,}表示匹配3次或者3次以上，
        // 由于前面(.)已经匹配过一次，所以整个限定次数比这个数字大1。
        return;
      }
      tempText.value = inputText.value;

      const res = await comment.add({
        postId,
        content: inputText.value
      });
      result(res).success(data => {
        message.success(data.msg);
        inputText.value = '';
        close();
        getList();
      });
    };

    const changeStatus = async (item, status) => {
      
      const res = await comment.updateStatus({
        id: item._id,
        status
      });
      result(res).success(({ msg }) => {
        message.success(msg);
        getList();
      });

    }

    const bySort = (status) => {
      if(status){
        getList(1);
      }else{
        getList();
      }
    }

    const toDetail = item => {
      Modal.confirm({
        content: (
          <div>
            <p>发布者：{item.author || "暂无"}</p>
            <p>评论内容：{item.content || "暂无"}</p>
            <p>发布时间：{formatTimestamp(item.meta.updatedAt) || "暂无"}</p>
          </div>
        )
      });
    };

    const onSearch = () => {
      // !! 转化成bool类型 隐式转化
      isSearch.value = !!keyword.value;

      const key = getIdByClassifyInfo(keyword.value);
      console.log(key);

      getList(key);
    };

    const backAll = () => {
      isSearch.value = false;
      keyword.value = "";
      getList();
    };


    return {
      changeLikes,
      inputText,
      list,
      total,
      submit,
      formatTimestamp,
      remove,
      changeStatus,
      bySort,
      toDetail,
      backAll,
      keyword,
      onSearch,
      isSearch
    };
  }

});
