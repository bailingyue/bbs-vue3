import {defineComponent, ref, onMounted} from 'vue';
import {createMemoryHistory, useRoute, useRouter} from 'vue-router';
import {
  post,
  logs
} from '@/service';
import {
  result,
  formatTimestamp,
  clone
} from '@/helpers/utils';
import {message} from 'ant-design-vue';
import Update from './Update/index.vue';
import {getClassifyInfoById} from '@/helpers/post-classify';
import {LikeOutlined, MessageOutlined } from "@ant-design/icons-vue";
import Comments from './comments/index.vue'
import store from '@/store';

const defalutInfo = {
  meta:{
    updatedAt:""
  },
  classify:''
}; 
export default defineComponent({
  components:{
    Update,
    LikeOutlined,
    MessageOutlined,
    Comments
  },

  setup(){
    // 拿到url的id
    const detailInfo = ref(clone(defalutInfo));
    const route = useRoute();
    const router = useRouter()
    const id = route.params.id; 

    const showUpdateModel = ref(false);
    const log = ref([]);
    const logTotal = ref(0);
    const logCurPage = ref(1);

    const getDetail = async () => {
      const res = await post.detail(id);
      result(res)
        .success(({data}) => {
          detailInfo.value = data;
        });
    };
    const remove = async () => {
      const res = await post.remove(id);
      result(res)
       .success(({msg}) => {
        message.success(msg);
        router.replace(`/datashow/post/${detailInfo.value.classify}`);
       })
    };
    const update = async (post) => {
      Object.assign(detailInfo.value, post)
    };

    const setLogPage = async (page) => {
      logCurPage.value = page
    };

    const toBack = () => {
      router.push(`/dashboard`);
    }

    onMounted(()=> {
      getDetail();
      store.commit('setRouteInfo', detailInfo.value.author);
    });
    return{
      d:detailInfo,
      formatTimestamp,
      remove,
      showUpdateModel,
      update,
      log,
      logTotal,
      setLogPage,
      toBack,
      logCurPage,
      getClassifyInfoById
    };
  },

  
});
