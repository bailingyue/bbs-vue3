import { defineComponent, ref, onMounted } from "vue";
import { inviteCode } from "@/service";
import { result, formatTimestamp } from "@/helpers/utils";
import { message } from "ant-design-vue";
// import {} from '@ant-design/icons-vue';

const columns = [
  {
    title: '邀请码',
    dataIndex: 'code'
  },
  {
    title: '使用状态',
    slots: {
        customRender: 'status'
      }
  },
  {
    title: '记录时间',
    slots: {
      customRender: 'createdAt'
    }
  },
  {
    title: '操作',
    slots: {
      customRender: 'actions'
    }
  },
]

export default defineComponent({
  components: {},
  // 在created()生命周期之前执行
  // 定义methods、watch、computed、data数据都放在函数中
  setup() {
    const curPage = ref(1);
    const total = ref(0);
    const list = ref([]);
    const showSize = ref(5);
    const count = ref(1);

    const getList = async () => {
      const res = await inviteCode.list({
        page: curPage.value,
        size: showSize.value
      });
      result(res).success(({ data: { total: T, list: L } }) => {
        // message.success(msg);
        list.value = L;
        total.value = T;
      });
    };

    const setPage = (page) => {
      curPage.value = page;
      getList();
    }

    const remove = async ({_id}) => {
      const res = await inviteCode.remove(_id)
      result(res)
       .success(({msg}) => {
         message.success(msg);
         getList();
       })
    }
    
    onMounted(() => {
      getList();
    });

    const add = async () => {
        
        const res = await inviteCode.add({
            count: count.value
        })
        result(res)
         .success(({msg}) => {
           message.success(msg);
           getList();
         })
    }

    return {
      curPage,
      total,
      getList,
      list,
      columns,
      showSize,
      setPage,
      formatTimestamp,
      remove,
      add,
      count
    };
  }
});


