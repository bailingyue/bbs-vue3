import { defineComponent, ref, onMounted } from "vue";
import { messages, post } from "@/service";
import { result, formatTimestamp } from "@/helpers/utils";
import { message, Modal } from "ant-design-vue";
import { NotificationFilled } from "@ant-design/icons-vue";
import { useRouter } from "vue-router";

const list_left = ["收到的赞", "我的消息"];

export default defineComponent({
  components: {
    NotificationFilled
  },
  setup() {
    const list = ref([]);

    const like_total = ref([]);
    const comment_total = ref([]);

    const content_title = ref("");
    const router = useRouter();

    const getLike = async e => {
      const res = await messages.like();
      result(res).success(({ data: { like_total: lt, like_list: ll } }) => {
        like_total.value = lt;
        list.value = ll;
        console.log(list.value);
        content_title.value = list_left[0];
      });
    };

    const getComment = async e => {
      const res = await messages.comment();
      result(res).success(({ data: { comment_total: ct, comment_list: cl } }) => {
        comment_total.value = ct;
        list.value = cl;
        content_title.value = list_left[1];
      });
    };

    const hide_content = async (item, type) => {
      const res = await messages.updateStatus({
        _id: item._id
      });
      result(res).success(() => {
        if (type == list_left[0]) {
          getLike();
        }
        if (type == list_left[1]) {
          getComment();
        }
        message.success("消息隐藏成功");
      });
    };

    const getCount = async () => {
      const res = await messages.count();
      result(res).success(({ data: { like_total: lt, comment_total: ct } }) => {
        like_total.value = lt;
        comment_total.value = ct;
      });
    };

    const toDetail = item => {
      if (item.type == "post_comment") {
        Modal.confirm({
          title: `回复的帖子：${item.title}`,
          content: (
            <div>
              <p>发布者：{item.sendUser || "暂无"}</p>
              <p>公告内容：{item.content || "暂无"}</p>
              <p>发布时间：{formatTimestamp(item.meta.updatedAt) || "暂无"}</p>
            </div>
          )
        });
      } else {
        router.push(`/dataShowDetail/${item.postId}/${item.classify}`);
      }
    };
    
    const showDetail = async item => {
      const res = await post.detail(item.postId);
      result(res).success(({ data }) => {
        if (!data) {
          message.error("该帖子已经被删除");
        } else {
          router.push(`/dataShowDetail/${item.postId}/${item.classify}`);
        }
      });
    };

    onMounted(() => {
      getCount();
      getLike();
    });

    return {
      like_total,
      comment_total,
      content_title,
      list,
      hide_content,
      getComment,
      getLike,
      toDetail,
      formatTimestamp,
      list_left,
      showDetail
    };
  }
});
