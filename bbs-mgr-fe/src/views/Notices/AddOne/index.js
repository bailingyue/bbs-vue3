import { defineComponent, reactive, ref, onMounted } from "vue";
import { notices } from "@/service";
import { result, clone } from "@/helpers/utils";
import { message } from "ant-design-vue";
import store from "@/store";

const defaultFormData = reactive({
  title: "",
  author: "",
  content: "",
  classify: ""
});

export default defineComponent({
  props: {
    show: Boolean
  },
  setup(props, context) {
    // 生拷贝防止reactive对其造成影响
    const addForm = reactive(clone(defaultFormData));
    // addForm.classify = store.state.postClassifies[0]._id;

    const submit = async () => {
      let form = clone(addForm);

      // 时间戳 1970年开始的秒数
      // form.date = addForm.date.valueOf();
      // 需要区分 是否为管理台传来的
      if(!form.title || !form.content || !form.author){
        message.error('标题、内容、作者不能为空')
        return;
      }
      Object.assign(form, {
        isManage:true
      })
      const res = await notices.add(form);
      result(res).success(data => {
        // 清空表单
        Object.assign(addForm, defaultFormData);
        // addForm.classify = store.state.postClassifies[0]._id;
        message.success(data.msg);
        close();
        context.emit("getList");
      });
    };

    const close = () => {
      context.emit("update:show", false);
    };

    return {
      addForm,
      submit,
      props,
      close,
      store: store.state
    };
  }
});
