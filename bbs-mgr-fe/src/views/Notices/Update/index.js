import { defineComponent, reactive, watch } from "vue";
import { notices } from "@/service";
import { result, clone } from "@/helpers/utils";
import { message } from "ant-design-vue";
// import moment from "moment";
import store from "@/store";

const defaultFormData = reactive({
  title: "",
  author: "",
  content: "",
  classify: ""
});
export default defineComponent({
  components: {},
  props: {
    show: Boolean,
    notice: Object
  },
  setup(props, context) {
    const editForm = reactive(clone(defaultFormData));

    const close = () => {
      context.emit("update:show", false);
    };

    watch(
      () => props.notice,
      current => {
        Object.assign(editForm, current);
        // 把时间戳转化为正常时间
        // editForm.date = moment(Number(editForm.date));
      }
    );

    const submit = async () => {
      if (
        !editForm.title ||
        !editForm.author ||
        !editForm.content
      ) {
        message.error("值不能为空");
        return;
      }
      // 字符不能为非法字符
      let acc_re = "^[A-Za-z0-9]+$";
      if(!editForm.author.match(acc_re)){
      　//由数字和26个英文字母组成的字符串
          message.info('账户名必须由数字或英文字母组成');
        return;  
      }
      // ...  合并对象
      const res = await notices.update({
        id: props.notice._id,
        title: editForm.title,
        author: editForm.author,
        content: editForm.content,
        classify: editForm.classify,
      });
      result(res).success(({ data, msg }) => {
        message.success(msg);
        context.emit("update", data);
        close();
      });
    };

    return {
      editForm,
      submit,
      props,
      close,
      store: store.state
    };
  }
});
