import { defineComponent, ref, onMounted } from "vue";
import { message, Modal } from "ant-design-vue";
import { notices } from "@/service";
import { result, formatTimestamp } from "@/helpers/utils";
import { useRouter } from "vue-router";
import addOne from "./AddOne/index.vue";
import update from "./Update/index.vue";
import { getClassifyInfoById, getIdByClassifyInfo } from "@/helpers/post-classify";
import { getCharacterInfoById } from "@/helpers/character";
import { getHeaders } from "@/helpers/request";
/*
    useRoute
      当前页面的路由
    useRouter
      当前页面的方法 如前进后退
  */
const columns = [
  {
    title: "名称",
    dataIndex: "title"
  },
  {
    title: "发布者",
    dataIndex: "author"
  },
  {
    title: "创建时间",
    slots: {
      customRender: "createdAt"
    }
  },
  {
    title: "所属板块",
    slots: {
      customRender: "classify"
    }
  },
  {
    title: "内容",
    dataIndex: "content",
    ellipsis: true
  },
  {
    title: "操作",
    slots: {
      customRender: "actions"
    }
  }
];
export default defineComponent({
  components: {
    addOne,
    update
  },

  setup() {
    const show = ref(false);
    const isSearch = ref(false);
    const list = ref([]);
    const curPage = ref(1);
    // const total = ref(0);
    const keyword = ref("");
    const showUpdateModel = ref(false);
    const curEditList = ref({});
    // const showSize = ref(5);

    const getList = async (keyword = '') => {
      const res = await notices.listAll({
        // page: curPage.value,
        // size: showSize.value,
        keyword
      });
      result(res).success(({ data }) => {
        list.value = data;
        // total.value = T;
      });
    };

    onMounted(() => {
      getList();
    });

    // const setPage = (page) => {
    //   curPage.value = page;
    //   getList();
    // }
    const onSearch = () => {
      // !! 转化成bool类型 隐式转化
      isSearch.value = !!keyword.value;

      const key = getIdByClassifyInfo(keyword.value);

      getList(key);
    };

    const backAll = () => {
      isSearch.value = false;
      keyword.value = "";
      getList();
    };

    const remove = async ({ text: record }) => {
      const { _id } = record;
      const res = await notices.remove(_id);
      result(res).success(({ msg }) => {
        message.success(msg);
        // 方法一
        getList();

        // 方法二
        // const idx = list.value.findIndex((item) => {
        //   return item._id === _id;
        // })
        // list.value.splice(idx, 1);
      });
    };

    const update = ({ record }) => {
      showUpdateModel.value = true;
      curEditList.value = record;
    };

    const updateCurList = newData => {
      Object.assign(curEditList.value, newData);
    };

    // 进入详情页
    const toDetail = ({ record }) => {
      Modal.confirm({
        title: `${record.title}`,
        content: (
          <div>
            <p>发布者：{record.author || "暂无"}</p>
            <p>公告内容：{record.content || "暂无"}</p>
            <p>所属板块：{getClassifyInfoById(record.classify).title}</p>
          </div>
          )
      });
    };

    const onUploadChange = ({ file }) => {
      if (file.status == "removed") {
        message.success("删除成功");
        return;
      }
      if (file.response) {
        result(file.response).success(async Key => {
          const res = await notices.addMany({ Key });
          result(res).success(({ msg }) => {
            message.success(msg);
            getList();
          });
        });
      }
    };

    const changeStatus = async ({ record }) => {
      const res = await notices.updateStatus({
        id: record._id,
        status: !record.status
      });
      result(res).success(({ msg }) => {
        message.success(msg);
        getList();
      });
    };

    return {
      columns,
      show,
      list,
      formatTimestamp,
      // curPage,
      // total,
      getList,
      keyword,
      isSearch,
      onSearch,
      backAll,
      remove,
      showUpdateModel,
      update,
      curEditList,
      updateCurList,
      toDetail,
      // showSize,
      getClassifyInfoById,
      onUploadChange,
      headers: getHeaders(),
      getCharacterInfoById,
      changeStatus,
      // setPage
    };
  }
});
