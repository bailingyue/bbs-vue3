import { defineComponent, ref, onMounted, reactive, watch} from "vue";
import { postClassify } from "@/service";
import { result, formatTimestamp,clone } from "@/helpers/utils";
import { message, Modal, Input} from "ant-design-vue";
import { RestFilled } from "@ant-design/icons-vue";
// import {} from '@ant-design/icons-vue';

const columns = [
  {
    title: '分类',
    dataIndex: 'title'
  },
  {
    title: '创建时间',
    slots: {
      customRender: 'createdAt'
    }
  },
  {
    title: '操作',
    slots: {
      customRender: 'actions'
    }
  },
]

export default defineComponent({

  // 在created()生命周期之前执行
  // 定义methods、watch、computed、data数据都放在函数中
  setup() {
    const curPage = ref(1);
    const total = ref(0);
    const showSize = ref(5);
    const title = ref('');

    const list =  ref([]);

    const showEditPlate = ref(false);
    const freeList = ref([]);
    const checkList = ref([]);


    const editForm = reactive({
      title: '',
      content: '',
      moderators: [],
      current:{}
    });

    const getList = async () => {
      const res = await postClassify.list({
        page: curPage.value,
        size: showSize.value
      });
      result(res).success(({ data: { total: T, list: L } }) => {
        // message.success(msg);
        list.value = L;
        total.value = T;
      });
    };

    const setPage = (page) => {
      curPage.value = page;
      getList();
    }

    const remove = async (record) => {
      const res = await postClassify.remove({
        id: record._id,
        moderators: record.moderators
      });
      result(res)
       .success(({msg}) => {
         message.success(msg);
         getList();
       })
    }

    onMounted(() => {
      getList();
    });

    const add = async () => {
      if(!title.value){
        message.error("分类不能为空")
        return;
      }
        const res = await postClassify.add({
            title: title.value
        })
        result(res)
         .success(({msg}) => {
           message.success(msg);
           getList();
         })
    }

    const onEdit = async (record) => {
      showEditPlate.value = true;
      editForm.current = record;
      
      editForm.title = record.title;
      editForm.content = record.content;
      editForm.moderators = record.moderators;
      checkList.value = record.moderators;

      // 可用版主列表
      const res = await postClassify.listModerator();
      result(res).success(({data}) => {
        for(let item of data){
          freeList.value.push(item.account)
        }
      });

      // 以防万一 前台需要去重处理
      editForm.moderators = editForm.moderators.concat(freeList.value);
      freeList.value = [];
    };

    const updateEditPlate = async () => {
      if(!editForm.title){
        message.error("标题不能为空")
        return;
      }

      const res = await postClassify.update({
        id: editForm.current._id,
        title: editForm.title,
        content: editForm.content  || '',
        moderators: checkList.value
      })

      result(res)
        .success(({
          msg
        }) => {
          message.success(msg);
          showEditPlate.value = false;
          getList();
        })
    }

    const showDetail = async (record) => {
      Modal.confirm({
        title: `${record.title}`,
        content: (
          <div>
            <p>版主共：{record.moderators.length}人</p>
            <p>版主：{record.moderators}</p>
            <p>板块介绍：{record.content || '暂无'}</p>
          </div>
          )
      });
    };


    return {
      curPage,
      total,
      getList,
      list,
      columns,
      showSize,
      setPage,
      formatTimestamp,
      remove,
      add,
      title,
      onEdit,
      showEditPlate,
      editForm,
      updateEditPlate,
      checkList,
      showDetail
    };
  }
});


