import {defineComponent, ref, onMounted} from 'vue';
import {createMemoryHistory, useRoute, useRouter} from 'vue-router';
import {
  post,
  logs
} from '@/service';
import {
  result,
  formatTimestamp
} from '@/helpers/utils';
import {message} from 'ant-design-vue';
import Update from '@/views/Posts/Update/index.vue';
import {getClassifyInfoById} from '@/helpers/post-classify';

const columns = [
  {
    title: '内容',
    dataIndex: 'content',
    ellipsis: true,
  },
  {
    title: '分类',
    slots: {
      customRender: 'classify'
    }
  },
  {
    title: '操作用户',
    dataIndex: 'account',
  },
  {
    title: '操作时间',
    slots: {
      customRender: 'createdAt'
    }
  }
]

export default defineComponent({
  components:{
    Update
  },

  setup(){

    // 拿到url的id
    const route = useRoute();
    const router = useRouter()
    const id = route.params.id; 
    const detailInfo = ref({});
    const showUpdateModel = ref(false);
    const log = ref([]);
    const logTotal = ref(0);
    const logCurPage = ref(1);


    const getDetail = async () => {
      const res = await post.detail(id);
      result(res)
        .success(({data}) => {
          detailInfo.value = data;
        });
    };
    const remove = async () => {
      const res = await post.remove(id);
      result(res)
       .success(({msg}) => {
        message.success(msg);
        router.replace('/posts');
       })
    };
    const update = async (post) => {
      Object.assign(detailInfo.value, post)
      // 更新日志
      getLogs();
    };

    const setLogPage = async (page) => {
      logCurPage.value = page
    };

    const getLogs = async () => {
      const res = await logs.list({
        postId: id,
        page: logCurPage.value,
        size: 10
      });
      result(res)
      .success(({ data: {
        total,
        list,
      }}) => {
        log.value = list;
        logTotal.value = total;

      });
    };

    const toBack = () => {
      router.replace('/posts');
    }
    onMounted(()=> {
      getDetail();
      getLogs();
    });
    return{
      d:detailInfo,
      formatTimestamp,
      remove,
      showUpdateModel,
      update,
      getLogs,
      log,
      logTotal,
      setLogPage,
      columns,
      toBack,
      logCurPage,
      getClassifyInfoById
    };
  },

  
});
