import { defineComponent, ref, onMounted, reactive } from "vue";
import { profile, userScore } from "@/service";
import { result, formatTimestampm, clone } from "@/helpers/utils";
import { message } from "ant-design-vue";
import store from "@/store";
// import {} from '@ant-design/icons-vue';

const defaultForm = {
  classify: "",
  content: ""
};

export default defineComponent({
  setup() {
    const { postClassifies } = store.state;
    const form = reactive(clone(defaultForm));
    const showScores = ref(0);
    try{
      form.classify = postClassifies[0]._id;
    }catch{
      form.classify = '';
    }

    const submit = async () => {
      if (!form.content) {
        message.error("理由不能为空");
        return;
      }
      // 保留中文信息
      let str_content = form.content.match('[\u4E00-\u9FA5]+');
      console.log(str_content);
      if(!str_content){
        message.error("申请理由非法");
        return;
      }
      if(str_content && str_content.length < 6){
        message.error("申请理由过于简单");
        return;
      }
      const res = await profile.apply({
        classify: form.classify,
        content: str_content
      });
      result(res).success(({ msg }) => {
        message.success(msg);
      });
      Object.assign(form, defaultForm);
      try{
        form.classify = postClassifies[0]._id;
      }catch{
        form.classify = '';
      }
  
    };

    const resetForm = async () => {
      Object.assign(form, defaultForm);
      try{
        form.classify = postClassifies[0]._id;
      }catch{
        form.classify = '';
      }
  
    };

    const getStore = async () => {
      const res = await userScore.score();
      result(res).success(({ data }) => {
        showScores.value = data.score;
      });
    };

    onMounted(() => {
      getStore();
    });

    return {
      form,
      resetForm,
      submit,
      postClassifies,
      showScores
    };
  }
});
