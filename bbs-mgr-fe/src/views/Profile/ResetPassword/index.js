import { defineComponent, ref, onMounted, reactive } from "vue";
import { profile } from "@/service";
import { result, formatTimestampm, clone } from "@/helpers/utils";
import { message } from "ant-design-vue";
import md5 from 'md5';
// import {} from '@ant-design/icons-vue';

const defaultForm = {
  oldPassword: "",
  newPassword: "",
  confirmNewPassword: ""
};

export default defineComponent({
  setup() {
    const form = reactive(clone(defaultForm));

    const submit = async () => {
      const old_ = form.oldPassword;
      const new_ = form.newPassword;
      const conf_ = form.confirmNewPassword;

      if (!old_ || !new_ || !conf_) {
        message.error("密码不能为空");
        return;
      }

      if (new_ != conf_) {
        message.error("新密码两次输入不一致");
        return;
      }
      let pas_re = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$";
      if(!form.newPassword.match(pas_re) || form.newPassword.length < 8){
        　message.info('新密码必须由数字和英文字母组成，长度为8-16');
        return;  
      }

      const res = await profile.update({
        oldPassword: old_,
        newPassword: md5(new_)
      });
      result(res).success(({ msg }) => {
        message.success(msg);
      });
      Object.assign(form, defaultForm);
    };

    const resetForm = async () => {
      Object.assign(form, defaultForm);
    };

    return {
      form,
      resetForm,
      submit
    };
  }
});
