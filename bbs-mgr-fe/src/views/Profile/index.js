import { defineComponent, ref, onMounted, reactive } from "vue";
// import {} from '@ant-design/icons-vue';
import ResetPassword from "./ResetPassword/index.vue";
import ApplyModerator from "./ApplyModerator/index.vue";


export default defineComponent({
  components: {
    ResetPassword,
    ApplyModerator
  },
  setup() {}
});
