import { defineComponent, ref, onMounted } from "vue";
import { records } from "@/service";
import { result, formatTimestamp } from "@/helpers/utils";
import { message } from "ant-design-vue";
// import {} from '@ant-design/icons-vue';
import { getLogInfoByPath } from '@/helpers/record';

const columns = [
  {
    title: '用户名',
    dataIndex: 'user.account'
  },
  {
    title: '动作',
    dataIndex:'action'
  },
  {
    title: '记录时间',
    slots: {
      customRender: 'createdAt'
    }
  },
  {
    title: '操作',
    slots: {
      customRender: 'action'
    }
  },
]

export default defineComponent({
  components: {},
  // 在created()生命周期之前执行
  // 定义methods、watch、computed、data数据都放在函数中
  setup() {
    const curPage = ref(1);
    const total = ref(0);
    const list = ref([]);
    const showSize = ref(5);
    const getList = async () => {
      const res = await records.list({
        page: curPage.value,
        size: showSize.value
      });
      result(res).success(({ data: { total: T, list: L } }) => {
        // message.success(msg);
        L.forEach( (item) => {
          item.action = getLogInfoByPath(item.request.url);
        })
        list.value = L;
        total.value = T;
      });
    };

    const setPage = (page) => {
      curPage.value = page;
      getList();
    }

    const remove = async ({_id}) => {
      const res = await records.remove(_id)
      result(res)
       .success(({data , msg}) => {
         message.success(msg);
         getList();
       })
    }
    
    onMounted(() => {
      getList();
    });

    return {
      curPage,
      total,
      getList,
      list,
      columns,
      showSize,
      setPage,
      formatTimestamp,
      remove
    };
  }
});

