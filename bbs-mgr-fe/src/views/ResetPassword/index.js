import { defineComponent, ref, onMounted } from "vue";
import { resetPassword } from "@/service";
import { result, formatTimestamp } from "@/helpers/utils";
import { message } from "ant-design-vue";
// import {} from '@ant-design/icons-vue';

const columns = [
  {
    title: '账号',
    dataIndex: 'account'
  },
  {
    title: '操作',
    slots: {
      customRender: 'actions'
    }
  },
  {
    title: '处理时间',
    slots: {
      customRender: 'createdAt'
    }
  },
]

export default defineComponent({
  components: {},
  // 在created()生命周期之前执行
  // 定义methods、watch、computed、data数据都放在函数中
  setup() {
    const curPage = ref(1);
    const total = ref(0);
    const list = ref([]);
    const showSize = ref(5);

    const getList = async () => {
      const res = await resetPassword.list({
        page: curPage.value,
        size: showSize.value
      });
      result(res).success(({ data: { total: T, list: L } }) => {
        // message.success(msg);
        list.value = L;
        total.value = T;
      });
    };

    const setPage = (page) => {
      curPage.value = page;
      getList();
    }
    
    onMounted(() => {
      getList();
    });

    const changeStatus = async ({_id, account}, status) => {
      
      const res = await resetPassword.update({
        id: _id,
        account,
        status
      });

      result(res).success(({msg }) => {
        message.success(msg);
        getList()
      });
    }
    
    return {
      curPage,
      total,
      getList,
      list,
      columns,
      showSize,
      setPage,
      formatTimestamp,
      changeStatus
    };
  }
});

