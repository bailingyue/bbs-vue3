import { defineComponent, reactive, ref, watch } from "vue";
import { user } from "@/service";
import { result, clone } from "@/helpers/utils";
import { message } from "ant-design-vue";
import store from "@/store";
import { getCharacterInfoById } from "@/helpers/character";
import md5 from 'md5';

const defaultFormData = {
  account: "",
  password: "",
  character: "",
  classify: ""
};

export default defineComponent({
  components: {},
  props: {
    show: Boolean
  },
  setup(props, context) {
    const { characterInfo, postClassifies } = store.state;
    const ManageShow = ref(false);
    // 生拷贝防止reactive对其造成影响
    const addForm = reactive(clone(defaultFormData));
    addForm.character = characterInfo[2]._id;

    watch(
      () => props.show,
      current => {
        if (current) {
          getCharacter(addForm.character)
        }
      }
    )

    watch(
      () => addForm.character,
      current => {
        if (current) {
          getCharacter(addForm.character)
        }
      }
    );

    const close = () => {
      context.emit("update:show", false);
    }
    const submit = async () => {
      const form = clone(addForm);
      if(!form.account || !form.password || !form.character){
        message.error("值不能为空");
        return;
      }

      // 字符不能为非法字符
      let acc_re = "^[A-Za-z0-9]+$";
      if(!form.account.match(acc_re)){
      　//由数字和26个英文字母组成的字符串
          message.info('账户名必须由数字或英文字母组成');
        return;  
      }

      let pas_re = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$";
      if(! form.password.match(pas_re) ||  form.password.length < 8){
        　message.info('密码必须由数字和英文字母组成，长度为8-16');
        return;  
      }

      form.password = md5(form.password);
      const res = await user.add(form);
      result(res).success(data => {
        // 清空表单
        Object.assign(addForm, defaultFormData);
        addForm.character = characterInfo[2]._id;
        message.success(data.msg);
        context.emit("update:show", false);
        context.emit("getUser");
        close();
      });
    };
    const getCharacter = id => {
      if (getCharacterInfoById(id).name == "moderator") {
        ManageShow.value = true;
      }else{
        ManageShow.value = false;
      }
    };

    return {
      addForm,
      submit,
      props,
      close,
      characterInfo,
      getCharacter,
      ManageShow,
      postClassifies
    };
  }
});
