import { defineComponent, ref, onMounted, reactive, watch } from "vue";
import { user } from "@/service";
import { result, formatTimestamp } from "@/helpers/utils";
import { message } from "ant-design-vue";
import AddOne from "./AddOne/index.vue";
import { getCharacterInfoById } from "@/helpers/character";
import { EditOutlined } from "@ant-design/icons-vue";
import store from "@/store";
import { getHeaders } from "@/helpers/request";

const columns = [
  {
    title: "账户",
    dataIndex: "account"
  },
  {
    title: "身份",
    slots: {
      customRender: "character"
    }
  },
  {
    title: "创建时间",
    slots: {
      customRender: "createdAt"
    }
  },
  {
    title: "操作",
    slots: {
      customRender: "actions"
    }
  }
];

export default defineComponent({
  components: {
    AddOne,
    EditOutlined
  },

  setup() {
    const { characterInfo, postClassifies } = store.state;
    const list = ref([]);
    const curPage = ref(1);
    const total = ref(0);
    const show = ref(false);
    const showSize = ref(5);
    const keyword = ref("");
    const isSearch = ref(false);
    const ManageShow = ref(false);
    const showEditCharacter = ref(false);

    const editForm = reactive({
      character: "",
      classify: "",
      current: {}
    });

    const getList = async () => {
      const res = await user.list({
        page: curPage.value,
        size: showSize.value,
        keyword: keyword.value
      });

      result(res).success(({ data: { total: T, list: L } }) => {
        list.value = L;
        total.value = T;
      });
    };

    onMounted(() => {
      getList();
    });

    watch(
      () => editForm.character,
      current => {
        if (current) {
          getCharacter(editForm.character);
        }
      }
    );

    watch(
      () => showEditCharacter.value,
      current => {
        if (current) {
          getCharacter(editForm.character);
        }
      }
    );

    const remove = async ({ _id }) => {
      const res = await user.remove(_id);
      result(res).success(({ msg }) => {
        message.success(msg);
        getList();
      });
    };

    const setPage = page => {
      curPage.value = page;
      getList();
    };

    const resetPassword = async ({ _id }) => {
      const res = await user.resetPassword(_id);
      result(res).success(({ msg }) => {
        message.success(msg);
      });
    };

    const onSearch = () => {
      // !! 转化成bool类型 隐式转化
      isSearch.value = !!keyword.value;
      getList();
    };
    const backAll = () => {
      isSearch.value = false;
      keyword.value = "";
      getList();
    };

    const onEdit = record => {
      editForm.current = record;
      editForm.character = record.character;
      editForm.classify = record.classify;
      showEditCharacter.value = true;
    };

    const updateCharacter = async () => {
      if (editForm.classify == editForm.current.classify && editForm.classify) {
        message.success("更新成功");
        showEditCharacter.value = false;
        return;
      }

      const res = await user.editCharacter({
        character: editForm.character,
        userId: editForm.current._id,
        classify: editForm.classify,
        oldclassify: editForm.current.classify,
        account: editForm.current.account
      });

      result(res).success(({ msg }) => {
        message.success(msg);
        showEditCharacter.value = false;
        getList();
      });
    };

    const getCharacter = id => {
      if (getCharacterInfoById(id).name == "moderator") {
        ManageShow.value = true;
        editForm.classify = editForm.current.classify;
      } else {
        ManageShow.value = false;
        editForm.classify = "";
      }
    };

    const onUploadChange = ({ file }) => {
      if (file.status == "removed") {
        message.success("删除成功");
        return;
      }
      if (file.response) {
        result(file.response).success(async Key => {
          const res = await user.addMany({ Key });
          result(res).success(({ msg }) => {
            message.success(msg);
            getList();
          });
        });
      }
    };

    return {
      getList,
      list,
      total,
      curPage,
      columns,
      formatTimestamp,
      remove,
      show,
      setPage,
      showSize,
      resetPassword,
      keyword,
      isSearch,
      onSearch,
      backAll,
      getCharacterInfoById,
      showEditCharacter,
      editForm,
      onEdit,
      characterInfo,
      updateCharacter,
      onUploadChange,
      headers: getHeaders(),
      postClassifies,
      ManageShow,
      getCharacter
    };
  }
});
